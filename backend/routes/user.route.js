import express from 'express';
import roleMiddleware from '../middlewares/role.middleware.js';
import adminRoutes from '../routes/admin.route.js';
import ownerRoutes from '../routes/owner.route.js';
import renterRoutes from '../routes/renter.route.js';

import userController from '../controllers/user.controller.js';
import accountMiddleware from '../middlewares/account.middleware.js';

const router = express.Router();

router.get('/admin', roleMiddleware.adminAuth);
router.get('/owner', roleMiddleware.ownerAuth);
router.get('/renter', roleMiddleware.renterAuth);
router.patch(
    '/change-password',
    accountMiddleware.userChangePassword,
    userController.changeUserPassword
);

router.use('/admin', roleMiddleware.adminAuth, adminRoutes);
router.use('/owner', roleMiddleware.ownerAuth, ownerRoutes);
router.use('/renter', roleMiddleware.renterAuth, renterRoutes);

export default router;
