import express from 'express';
const router = express.Router();
import ownerController from '../controllers/owner.controller.js';

import roleMiddleware from '../middlewares/role.middleware.js';
import postMiddleware from '../middlewares/post.middleware.js';
import accountMiddleware from '../middlewares/account.middleware.js';

import upload from '../config/multer.js';
// const uploads = multer({ dest: 'static/uploads' });

router.get('/info', ownerController.getAccountInfo);
router.put(
    '/info',
    roleMiddleware.editAccountAuth,
    accountMiddleware.userChangeInfo,
    ownerController.putAccountInfo
);
router.post(
    '/post',
    upload.array('image', 3),
    postMiddleware.createPost,
    ownerController.postCreatePost
);
router.get('/posts', ownerController.getAllPosts);
router.patch('/status-rented/:id', ownerController.patchStatusRented);
router.get('/un-posts', ownerController.getPostsUnappoved);
router.get('/edit/:id', ownerController.getPostById);
router.put(
    '/edit/:id',
    postMiddleware.editPost,
    ownerController.putEditPostById
);
router.get('/extend', ownerController.getExpiredPosts);
router.put('/extend/:id', ownerController.putExpiredPostById);
router.get('/notif', ownerController.getNotifications);
router.patch('/notif/:id', ownerController.patchNotification);

export default router;
