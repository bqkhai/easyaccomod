import express from 'express';
import { login } from '../controllers/login.controller.js';
import authMiddleware from '../middlewares/auth.middleware.js';
const router = express.Router();

router.post('/', authMiddleware.loginMiddleware, login);

export default router;
