import express from 'express';
const router = express.Router();

import renterController from '../controllers/renter.controller.js';

router.get('/search', renterController.getSearch);
router.get('/posts', renterController.getAllPosts);
router.get('/posts/:id', renterController.getPostById);
router.get('/favourite', renterController.getFavouritePosts);
router.patch('/favourite/:id', renterController.patchFavouritePost);
router.delete('/favourite/:id', renterController.delFavouritePost);
router.post('/review/:id', renterController.postReviewByPostId);
router.get('/reviews/:id', renterController.getAllReviews);
router.post('/report/:id', renterController.postReportByPostId);

export default router;
