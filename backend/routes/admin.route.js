import express from 'express';
const router = express.Router();

import adminController from '../controllers/admin.controller.js';

router.get('/un-accounts', adminController.getUnapprovedAccount);
router.get('/accounts', adminController.getAllAccounts);
router.patch('/accounts/:id', adminController.patchApprovedAccount);
router.get('/un-posts', adminController.getUnapprovedPost);
router.get('/un-posts/:id', adminController.getUnapprovedPostByID);
router.get('/posts', adminController.getAllPosts);
router.get('/posts/:id', adminController.getPostById);
router.patch('/posts/:id', adminController.patchApprovedPost);
router.get('/edit-auth', adminController.getUnEditableAccount);
router.patch('/edit-auth/:id', adminController.patchEditableAccount);
router.get('/reports', adminController.getAllReports);
router.get('/notif', adminController.getNotifications);
router.patch('/notif/:id', adminController.patchNotification);
router.get('/reports', adminController.getAllReports);
router.get('/reviews', adminController.getAllReviews);
router.patch('/reviews/:id', adminController.patchReviewById);

export default router;
