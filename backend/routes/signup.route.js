import express from 'express';
import { signup } from '../controllers/signup.controller.js';
import authMiddleware from '../middlewares/auth.middleware.js';
const router = express.Router();

router.post('/', authMiddleware.signupMiddleware, signup);

export default router;
