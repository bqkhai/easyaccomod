import { User } from '../models/user.model.js';

const signupMiddleware = async (req, res, next) => {
    let errors = [];
    const email_re =
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    const phone_re = /^[0-9]{10}$/;
    const id_card_number_re = /^[a-zA-Z0-9]{12}$/;
    const password_re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{6,20}$/;
    const name_re =
        /^[0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (!req.body.email) {
        errors.push('Email không được trống');
    }

    if (req.body.email && !req.body.email.match(email_re)) {
        errors.push('Sai email');
    }

    if (!req.body.name) {
        errors.push('Không được để trống');
    }

    if (req.body.name && !req.body.name.match(name_re)) {
        errors.push('Sai định dạng tên');
    }

    if (req.body.role === 'owner') {
        if (!req.body.id_card_number) {
            errors.push('CCCD không được để trống');
        }

        if (
            req.body.id_card_number &&
            !req.body.id_card_number.match(id_card_number_re)
        ) {
            errors.push('Căn cước công dân phải có 12 chữ số');
        }

        if (!req.body.phone) {
            errors.push('SĐT không được để trống');
        }

        if (req.body.phone && !req.body.phone.match(phone_re)) {
            errors.push('SĐT phải có 10 chữ số');
        }

        if (!req.body.address) {
            errors.push('Địa chỉ không được để trống');
        }

        if (req.body.address && req.body.address.length >= 255) {
            errors.push('Địa chỉ ít hơn 255 kí tự');
        }
    }

    if (!req.body.password) {
        errors.push('Mât khẩu không được để trống');
    }

    if (
        (req.body.password && !req.body.password.match(password_re)) ||
        (req.body.cf_pass && !req.body.cf_pass.match(password_re))
    ) {
        errors.push('Mật khẩu có từ 6-20 kí tự');
    }

    if (req.body.password !== req.body.cf_pass) {
        errors.push('Mật khẩu xác nhận không khớp');
    }

    try {
        const user = await User.findOne({ email: req.body.email });
        console.log(user);
        if (user) {
            errors.push('Email này đã tồn tại');
        }
    } catch (err) {
        res.status(500).send('server error');
        return;
    }

    if (errors.length) {
        const data = {
            errors: errors,
        };
        res.status(200).json(data);
        return;
    }

    next();
};

const loginMiddleware = async (req, res, next) => {
    let errors = [];
    let user;

    try {
        user = await User.findOne({ email: req.body.email });
    } catch (err) {
        res.status(500).send('server error');
    }

    const email_re =
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    const password_re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{6,13}$/;

    if (!req.body.email) {
        errors.push('Email không được để trống');
    }
    if (req.body.email && !req.body.email.match(email_re)) {
        errors.push('Email không đúng định dạng');
    }
    if (!req.body.password) {
        errors.push('Mật khẩu không được để trống');
    }
    if (req.body.password && !req.body.password.match(password_re)) {
        errors.push('Mật khẩu không đúng định dạng');
    }

    if (errors.length) {
        const data = {
            errors: errors,
        };
        res.status(200).json(data);
        return;
    }

    next();
};

export default {
    signupMiddleware,
    loginMiddleware,
};
