import fs from 'fs';

const createPost = (req, res, next) => {
    let errors = [];

    const string_re =
        /^[0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    const area_re = /^[1-9][0-9]*[,.]{0,1}[0-9]*$/;
    const money_re = /^[1-9][0-9]*$/;
    const time_post_re = /^[1-4]/;

    if (!req.body.title) {
        errors.push('Tiêu đề không được bỏ trống');
    }
    if (req.body.title && !req.body.title.match(string_re)) {
        errors.push('Trường này không đúng định dạng');
    }

    if (!req.body.city) {
        errors.push('Thành phố không được bỏ trống');
    }
    if (req.body.city && !req.body.city.match(string_re)) {
        errors.push('Trường này không đúng định dạng');
    }

    if (!req.body.district) {
        errors.push('Quận không được bỏ trống');
    }
    if (req.body.district && !req.body.district.match(string_re)) {
        errors.push('Trường này không đúng định dạng');
    }

    if (!req.body.ward) {
        errors.push('Phường không được bỏ trống');
    }
    if (req.body.ward && !req.body.ward.match(string_re)) {
        errors.push('Trường này không đúng định dạng');
    }

    if (!req.body.street) {
        errors.push('Đường không được bỏ trống');
    }
    if (req.body.street && !req.body.street.match(string_re)) {
        errors.push('Trường này không đúng định dạng');
    }

    if (!req.body.room_type) {
        errors.push('Loại phòng không được bỏ trống');
    }
    if (
        req.body.room_type &&
        req.body.room_type !== 'Nhà nguyên căn' &&
        req.body.room_type !== 'Chung cư mini' &&
        req.body.room_type !== 'Chung cư' &&
        req.body.room_type !== 'Phòng trọ' &&
        req.body.room_type !== 'Nhà trọ giá rẻ'
    ) {
        errors.push('Dữ liệu sai định dạng');
    }

    if (!req.body.room_price) {
        errors.push('Giá phòng không được bỏ trống');
    }
    if (req.body.room_price && !req.body.room_price.match(money_re)) {
        errors.push('Giá thuê cần là số');
    }

    if (!req.body.room_area) {
        errors.push('Diện tích không được bỏ trống');
    }
    if (req.body.room_area && !req.body.room_area.match(area_re)) {
        errors.push('Diện tích là số');
    }

    if (!req.body.electricity_price) {
        errors.push('Giá điện không được bỏ trống');
    }
    if (
        req.body.electricity_price &&
        !req.body.electricity_price.match(money_re)
    ) {
        errors.push('Giá điện cần là số');
    }

    if (!req.body.water_price) {
        errors.push('Giá nước không được bỏ trống');
    }
    if (req.body.water_price && !req.body.water_price.match(money_re)) {
        errors.push('Giá nước cần là số');
    }

    if (!req.body.time_post) {
        errors.push('Thời gian đnăg không được bỏ trống');
    }
    if (req.body.time_post && !req.body.time_post.match(time_post_re)) {
        errors.push('Số tuần đăng từ 1-4');
    }

    if (
        (req.files && req.files.length < 3) ||
        (req.files && req.files.length > 5) ||
        !req.files
    ) {
        errors.push('Cho phép thêm 3-5 ảnh');
        req.files.map((file) => {
            fs.unlinkSync(file.path);
        });
    }

    if (errors.length) {
        if (req.files) {
            req.files.map((file) => {
                fs.unlinkSync(file.path);
            });
        }
        const data = {
            errors: errors,
        };
        res.status(200).json(data);
        return;
    }
    next();
};

const editPost = (req, res, next) => {
    let errors = [];

    const string_re =
        /^[0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    const area_re = /^[1-9][0-9]*[,.]{0,1}[0-9]*$/;
    const money_re = /^[1-9][0-9]*$/;
    const time_post_re = /^[1-4]/;

    if (!req.body.title) {
        errors.push('Tiêu đề không được bỏ trống');
    }
    if (req.body.title && !req.body.title.match(string_re)) {
        errors.push('Trường này không đúng định dạng');
    }

    if (!req.body.city) {
        errors.push('Thành phố không được bỏ trống');
    }
    if (req.body.city && !req.body.city.match(string_re)) {
        errors.push('Trường này không đúng định dạng');
    }

    if (!req.body.district) {
        errors.push('Quận không được bỏ trống');
    }
    if (req.body.district && !req.body.district.match(string_re)) {
        errors.push('Trường này không đúng định dạng');
    }

    if (!req.body.ward) {
        errors.push('Phường không được bỏ trống');
    }
    if (req.body.ward && !req.body.ward.match(string_re)) {
        errors.push('Trường này không đúng định dạng');
    }

    if (!req.body.street) {
        errors.push('Đường không được bỏ trống');
    }
    if (req.body.street && !req.body.street.match(string_re)) {
        errors.push('Trường này không đúng định dạng');
    }

    if (!req.body.room_type) {
        errors.push('Loại phòng không được bỏ trống');
    }
    if (
        req.body.room_type &&
        req.body.room_type !== 'Nhà nguyên căn' &&
        req.body.room_type !== 'Chung cư mini' &&
        req.body.room_type !== 'Chung cư' &&
        req.body.room_type !== 'Phòng trọ' &&
        req.body.room_type !== 'Nhà trọ giá rẻ'
    ) {
        errors.push('Dữ liệu sai định dạng');
    }

    if (!req.body.room_price) {
        errors.push('Giá phòng không được bỏ trống');
    }
    if (req.body.room_price && !req.body.room_price.match(money_re)) {
        errors.push('Giá thuê cần là số');
    }

    if (!req.body.room_area) {
        errors.push('Diện tích không được bỏ trống');
    }
    if (req.body.room_area && !req.body.room_area.match(area_re)) {
        errors.push('Diện tích là số');
    }

    if (!req.body.electricity_price) {
        errors.push('Giá điện không được bỏ trống');
    }
    if (
        req.body.electricity_price &&
        !req.body.electricity_price.match(money_re)
    ) {
        errors.push('Giá điện cần là số');
    }

    if (!req.body.water_price) {
        errors.push('Giá nước không được bỏ trống');
    }
    if (req.body.water_price && !req.body.water_price.match(money_re)) {
        errors.push('Giá nước cần là số');
    }

    if (!req.body.time_post) {
        errors.push('Thời gian đnăg không được bỏ trống');
    }
    if (req.body.time_post && !req.body.time_post.match(time_post_re)) {
        errors.push('Số tuần đăng từ 1-4');
    }

    if (errors.length) {
        const data = {
            errors: errors,
        };
        res.status(200).json(data);
        return;
    }
    next();
};

export default {
    createPost,
    editPost,
};
