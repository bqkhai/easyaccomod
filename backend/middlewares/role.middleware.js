import { User } from '../models/user.model.js';

const adminAuth = async (req, res, next) => {
    const id = req.signedCookies.user_id;
    try {
        const user = await User.findById(id);
        if (!user) {
            res.status(400).json({ message: 'user not found' });
            return;
        }
        if (user && user.role === 'admin') {
            next();
        } else {
            return res.status(401).json('Access denied');
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
        return;
    }
};

const ownerAuth = async (req, res, next) => {
    const id = req.signedCookies.user_id;
    try {
        const user = await User.findById(id);
        // console.logs(user);
        if (!user) {
            res.status(400).json({ message: 'user not found' });
            return;
        }
        if (user && user.role === 'owner') {
            if (user.is_approved === true) next();
            else return res.status(401).json('Access denied');
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
        return;
    }
};

const renterAuth = async (req, res, next) => {
    const id = req.signedCookies.user_id;
    try {
        const user = await User.findById(id);
        if (!user) {
            res.status(400).json({ message: 'user not found' });
            return;
        }
        if (user && user.role === 'renter') {
            next();
        } else {
            return res.status(401).json('Access denied');
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
        return;
    }
};

const editAccountAuth = async (req, res, next) => {
    try {
        const user_id = req.signedCookies.user_id;
        const user = await User.findById(user_id);
        if (user.editable === false) {
            return res.status(401).json('Access denied');
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
        return;
    }
    next();
};

export default {
    adminAuth,
    ownerAuth,
    renterAuth,
    editAccountAuth,
};
