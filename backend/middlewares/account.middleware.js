import { User } from '../models/user.model.js';
import bcrypt from 'bcrypt';

const userChangeInfo = (req, res, next) => {
    let errors = [];

    const email_re =
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    const name_re =
        /^[0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    const address_re =
        /^[0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;

    if (!req.body.email) {
        errors.push('Email không được để trống');
    }
    if (req.body.email && !req.body.email.match(email_re)) {
        errors.push('Email không đúng định dạng');
    }
    if (!req.body.name) {
        errors.push('Tên không được để trống');
    }

    if (req.body.name && !req.body.name.match(name_re)) {
        errors.push('Tên sai định dạng');
    }
    if (
        req.body.name &&
        (req.body.name.length < 5 || req.body.name.length > 50)
    ) {
        errors.push('Tên tối thiểu 6 kí tự và ít hơn 50 kí tự');
    }
    if (!req.body.id_card_number) {
        errors.push('CCCD không được để trống');
    }
    if (req.body.id_card_number && req.body.id_card_number.length !== 12) {
        errors.push('Căn cước công dân phải có 12 chữ số');
    }
    if (!req.body.phone) {
        errors.push('SĐT không được để trống');
    }
    if (req.body.phone && req.body.phone.length !== 10) {
        errors.push('Số điện thoại phải có 10 chữ số');
    }
    if (!req.body.address) {
        errors.push('Địa chỉ không được để trống');
    }
    if (req.body.address && !req.body.address.match(address_re)) {
        errors.push('Địa chỉ sai định dạng');
    }

    next();
};

const userChangePassword = async (req, res, next) => {
    const user_id = req.signedCookies.user_id;
    const current_password = bcrypt.hash(req.body.current_password, 10);
    const password_re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{6,20}$/;

    let user;
    let errors = [];

    try {
        user = await User.findById(user_id);
    } catch (err) {
        res.status(500).send('server error');
        return;
    }

    if (
        !req.body.current_password ||
        !req.body.new_password ||
        !req.body.confirm_password
    ) {
        errors.push('Mật khẩu không được trống');
    }

    if (req.body.current_password && current_password !== user.password) {
        errors.push('Mật khẩu sai');
    }

    if (
        (req.body.current_password &&
            !req.body.current_password.match(password_re)) ||
        (req.body.new_password && !req.body.new_password.match(password_re)) ||
        (req.body.confirm_password &&
            !req.body.confirm_password.match(password_re))
    ) {
        errors.push('Mật khẩu cần có từ 6-20 kí tự');
    }

    if (req.body.new_password !== req.body.confirm_password) {
        errors.push('Mật khẩu xác nhận không khớp');
    }

    if (errors.length) {
        const data = {
            errors: errors,
        };
        res.status(200).json(data);
        return;
    }

    next();
};

export default {
    userChangeInfo,
    userChangePassword,
};
