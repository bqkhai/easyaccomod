import { User } from '../models/user.model.js';
import { Post } from '../models/post.model.js';
import { Review } from '../models/review.model.js';
import { Report } from '../models/report.model.js';
import { FavouritePost } from '../models/favouritePost.model.js';
import moment from 'moment';

/**
 * @controller GET search room
 * @route user/renter/search
 * Tìm kiếm phòng
 */
const getSearch = async (req, res) => {
    const data = {
        ...req.query,
        is_approved: true,
        status: true,
    };
    Post.find(data)
        .then((posts) => {
            let data = [];
            posts.forEach((post) => {
                const weeks_expired = post.time_post;
                const approved_time = post.approve_date;
                const time_diff = moment
                    .duration(moment() - approved_time)
                    .asWeeks();
                if (time_diff <= weeks_expired) {
                    data.push(post);
                }
            });
            res.status(200).json(data);
        })
        .catch((error) => res.status(500).send('server error'));

    // const data = {
    //     ...req.query,
    //     is_approved: true,
    //     rented_status: true,
    // };
    // try {
    //     const posts = await Post.find(data);
    //     let data = [];
    //     posts.forEach((post) => {
    //         const weeks_expired = post.time_post;
    //         const approved_time = post.approve_date;
    //         const time_diff = moment
    //             .duration(moment() - approved_time)
    //             .asWeeks();
    //         if (time_diff <= weeks_expired) {
    //             data.push(post);
    //         }
    //     });
    //     res.status(200).json(data);
    // } catch (err) {
    //     console.log(err);
    //     res.status(500).json({ success: false, message: err.message });
    // }
};

/**
 * @controller GET all post for renter
 * @route user/renter/posts
 * Lấy danh sách bài đăng
 */
const getAllPosts = async (req, res) => {
    try {
        const posts = await Post.find({ is_approved: true });
        let data = [];
        posts.forEach((post) => {
            const weeks_expired = post.time_post;
            const approved_time = post.approve_date;
            const time_diff = moment
                .duration(moment() - approved_time)
                .asWeeks();
            if (time_diff <= weeks_expired) {
                data.push(post);
            }
        });
        res.status(200).json(data);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

/**
 * @controller GET post by id
 * @route user/renter/posts/:id
 * Lấy bài đăng theo id
 */
const getPostById = async (req, res) => {
    const post_id = req.params.id;

    try {
        const post = await Post.findByIdAndUpdate(post_id, {
            $inc: { views: 1 },
        }).populate('user_id');
        // console.log(post);
        res.status(200).json(post);
    } catch (err) {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err.message,
        });
    }
};

/**
 * @controller GET favourite posts of renter
 * @route user/renter/favourite
 * Lấy danh sách yêu thích của renter
 */
const getFavouritePosts = async (req, res) => {
    const user_id = req.signedCookies.user_id;

    try {
        const favourite = await FavouritePost.findOne({
            user_id: user_id,
        }).populate('post_list');
        // console.log(favourite);
        res.status(200).json(favourite.post_list);
    } catch (err) {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err.message,
        });
    }
};

/**
 * @controller PATCH favourite post by id
 * @route user/renter/favourite/:id
 * Thêm bài post vào d/s yêu thích
 */
const patchFavouritePost = async (req, res) => {
    const user_id = req.signedCookies.user_id;
    const post_id = req.params.id;

    try {
        const post = await Post.findByIdAndUpdate(post_id, {
            $inc: { likes: 1 },
        });
        const favouritePost = await FavouritePost.findOneAndUpdate(
            { user_id: user_id },
            {
                $push: { post_list: post_id },
            }
        );
        res.status(200).send('success');
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

/**
 * @controller PATCH favourite post by id
 * @route user/renter/favourite/:id
 * Xóa bài đăng khỏi danh sách yêu thích
 */
const delFavouritePost = async (req, res) => {};

/**
 * @controller POST review
 * @route user/renter/review/:id
 * Review bài đăng
 */
const postReviewByPostId = async (req, res) => {
    const data = {
        user_id: req.signedCookies.user_id,
        post_id: req.params.id,
        star: req.body.star,
        review: req.body.review,
        is_approved: false,
        time: moment(),
    };
    const reviewData = new Review(data);
    try {
        await reviewData.save();
        res.status(201).send('success');
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

/**
 * @controller GET all reviews of post
 * @route user/renter/review/:id
 * Lấy danh sách review bài đăng
 */
const getAllReviews = async (req, res) => {
    try {
        const post_id = req.params.id;
        const reviews = await Review.find({
            post_id: post_id,
            is_approved: true,
        }).populate('user_id');
        res.status(200).json(reviews);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

/**
 * @controller POST report
 * @route user/renter/report/:id
 * Báo cáo bài đăng (vi phạm, lỗi, ko đúng ...) cho admin
 */
const postReportByPostId = async (req, res) => {
    const data = {
        user_id: req.signedCookies.user_id,
        post_id: req.params.id,
        report: req.body.report,
        time: moment(),
    };
    const reportData = new Report(data);
    try {
        await reportData.save();
        res.status(201).json(reportData);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

export default {
    getSearch,
    getAllPosts,
    getPostById,
    getFavouritePosts,
    patchFavouritePost,
    delFavouritePost,
    postReviewByPostId,
    getAllReviews,
    postReportByPostId,
};
