import { User } from '../models/user.model.js';
import { FavouritePost } from '../models/favouritePost.model.js';
import bcrypt from 'bcrypt';

export const signup = async (req, res) => {
    const userData = {
        ...req.body,
    };

    if (userData.role === 'owner') {
        userData.is_approved = false;
        userData.editable = false;
    }
    const hashPassword = await bcrypt.hash(req.body.password, 10);
    userData.password = hashPassword;

    try {
        const newUser = new User(userData);
        await newUser.save();
        if (req.body.role === 'renter') {
            const newFavouritePost = await new FavouritePost({
                user_id: newUser._id,
                postList: [],
            }).save();
        }
        res.status(201).json(newUser);
    } catch (err) {
        res.status(500).json({
            success: false,
            message: err.message,
            data: userData,
        });
    }
};
