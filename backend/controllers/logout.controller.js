export const logout = (req, res) => {
    res.clearCookie('user_id');
    res.clearCookie('user_role');
    res.status(200).json({ success: true, message: 'Logout success' });
};
