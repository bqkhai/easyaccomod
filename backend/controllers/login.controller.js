import { User } from '../models/user.model.js';
import { FavouritePost } from '../models/favouritePost.model.js';
import bcrypt from 'bcrypt';

export const login = async (req, res) => {
    let user;

    try {
        user = await User.findOne({ email: req.body.email });
        if (!user) {
            return res
                .status(400)
                .json({ message: 'Invalid email or password or role' });
        }
        const validPassword = await bcrypt.compare(
            req.body.password,
            user.password
        );
        if (!validPassword) {
            return res
                .status(400)
                .json({ message: 'Invalid email or password or role' });
        }
        const validRole = user._doc.role;
        if (req.body.role !== validRole) {
            return res
                .status(400)
                .json({ message: 'Invalid email or password or role' });
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }

    let data = {
        ...user._doc,
    };
    console.log(data);

    const user_role = user._doc.role;
    console.log(user_role);
    if (user_role === 'renter') {
        const favouritePost = await FavouritePost.findOne({
            user_id: user._doc._id,
        });
        data.favourite = favouritePost.post_list;
    }

    console.log(data);
    // response cookie
    res.cookie('user_role', user_role, {
        signed: true,
        maxAge: 24 * 60 * 60 * 1000,
    });
    res.cookie('user_id', user._doc._id, {
        signed: true,
        maxAge: 24 * 60 * 60 * 1000,
    });
    res.status(200).json(data);
};
