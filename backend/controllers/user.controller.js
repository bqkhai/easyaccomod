import { User } from '../models/user.model.js';
import bcrypt from 'bcrypt';

/**
 * @controller change password user
 * @route user/change-password
 */
const changeUserPassword = async (req, res) => {
    const user_id = req.signedCookies.user_id;

    const new_password = await bcrypt.hash(req.body.new_password, 10);

    try {
        await User.findByIdAndUpdate(user_id, { password: new_password });
        res.status(200).send('success');
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

export default { changeUserPassword };
