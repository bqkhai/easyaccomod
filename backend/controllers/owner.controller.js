import { User } from '../models/user.model.js';
import { Post } from '../models/post.model.js';
import { Notification } from '../models/notif.model.js';
import moment from 'moment';
import cloudinary from '../config/cloudinary.js';
// import upload from '../config/multer.js';

// @controller GET account info
// user/owner/info
// Lấy thông tin tài khoản owner
const getAccountInfo = async (req, res) => {
    const owner_id = req.signedCookies.user_id;
    console.log(owner_id);
    try {
        const user = await User.findById(owner_id);
        res.status(200).json(user);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// @controller PUT account info
// user/owner/info
// Sửa thông tin tài khoản owner
const putAccountInfo = async (req, res) => {
    const owner_id = req.signedCookies.user_id;
    try {
        const data = { ...req.body, is_approved: false };
        await User.findByIdAndUpdate(owner_id, data);
        res.status(200).json(data);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// @controller POST create new post
// user/owner/post
// Tạo bài post mới
// const postCreatePost = async (req, res) => {
//     const owner_id = req.signedCookies.user_id;

//     let images = [];
//     req.files.map((file) => {
//         console.log(file);
//         const file_dir = file.path.split('\\').slice(1).join('\\');
//         console.log(file_dir);
//         images.push(file_dir);
//     });

//     console.log(images);

//     const postData = {
//         ...req.body,
//         user_id: owner_id,
//         is_approved: false,
//         rented_status: false,
//         likes: 0,
//         views: 0,
//         image: images,
//     };
//     console.log(postData);
//     try {
//         const newPost = new Post(postData);
//         await newPost.save();
//         res.status(201).json(newPost);
//     } catch (err) {
//         console.log(err);
//         res.status(500).json('Server error');
//     }
// };

const postCreatePost = async (req, res) => {
    const owner_id = req.signedCookies.user_id;

    const images = [];
    const files = req.files;
    for (const file of files) {
        const { path } = file;
        const newPath = await cloudinary.uploader.upload(path);
        images.push(newPath);
    }

    const urls = [];
    images.map((image) => {
        urls.push(image.secure_url);
    });

    const postData = {
        ...req.body,
        user_id: owner_id,
        is_approved: false,
        rented_status: false,
        likes: 0,
        views: 0,
        image: urls,
    };
    console.log(postData);
    try {
        const newPost = new Post(postData);
        await newPost.save();
        res.status(201).json(newPost);
    } catch (err) {
        console.log(err);
        res.status(500).json('Server error');
    }
};

// @controller GET list posts
// user/owner/posts
// Lấy danh sách bài post của owner
const getAllPosts = async (req, res) => {
    const owner_id = req.signedCookies.user_id;
    try {
        const posts = await Post.find({ user_id: owner_id });
        res.status(200).json(posts);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// @controller PATCH status rented
// user/owner/status-rented/:id
// Thay đổi trạng thái (đã thuê/ chưa thuê) phòng
const patchStatusRented = async (req, res) => {
    const post_id = req.params.id;
    const user_id = req.signedCookies.user_id;
    try {
        let post = await Post.findById(post_id);
        post.rented_status = !post.rented_status;
        await post.save();

        const notification = {
            user_id: user_id,
            post_id: post_id,
            is_read: false,
            time: moment(),
            for: 'admin',
        };

        if (post.rented_status === true) {
            notification.title = ' đã có người thuê';
        } else {
            notification.title = ' chưa có người thuê';
        }

        const notificationForAdmin = new Notification(notification);
        await notificationForAdmin.save();

        res.status(200).json(post);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// @controller GET list posts unapproved
// user/owner/un-posts
// Lấy danh sách bài post chưa duyệt của owner
const getPostsUnappoved = async (req, res) => {
    const owner_id = req.signedCookies.user_id;
    try {
        const posts = await Post.find({
            user_id: owner_id,
            is_approved: false,
        });
        res.status(200).json({ success: true, data: posts });
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// @controller GET post by id
// user/owner/edit/:id
// Lấy bài post theo id
const getPostById = async (req, res) => {
    const post_id = req.params.id;
    try {
        const post = await Post.findById(post_id);
        res.status(200).json(post);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// @controller PUT post by id
// user/owner/posts/:id
// Sửa bài post theo id
const putEditPostById = async (req, res) => {
    const post_id = req.params.id;
    try {
        const data = {
            ...req.body,
            is_approved: false,
        };
        const post = await Post.findByIdAndUpdate(post_id, data);
        res.status(200).json(post);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// @controller GET expired posts list
// user/owner/extend
// Lấy danh sách bài đăng quá hạn
const getExpiredPosts = async (req, res) => {
    const user_id = req.signedCookies.user_id;

    try {
        const posts = await Post.find({ user_id: user_id, is_approved: true });

        let data = [];
        posts.forEach((post) => {
            const weeks_expired = post.time_post;
            const approved_time = post.approve_date;
            const time_diff = moment
                .duration(moment() - approved_time)
                .asWeeks();
            if (time_diff > weeks_expired) {
                data.push(post);
            }
        });
        res.status(200).json(data);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// Gia hạn bài đăng
const putExpiredPostById = async (req, res) => {
    const post_id = req.params.id;
    try {
        const data = {
            ...req.body,
            is_approved: false,
        };
        const post = await Post.findByIdAndUpdate(post_id, data);
        res.status(200).json(post);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

const getNotifications = async (req, res) => {
    const user_id = req.signedCookies.user_id;

    try {
        const notifs = await Notification.find({
            for: 'owner',
            user_id: user_id,
            is_read: false,
        })
            .populate('user_id', 'name')
            .populate('post_id', 'title')
            .sort({ time: -1 });
        console.log(notifs);
        res.status(200).json(notifs);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// user/owner/notif/:id
const patchNotification = async (req, res) => {
    const notif_id = req.params.id;
    try {
        await Notification.findByIdAndUpdate(notif_id, { is_read: true });
        res.status(200).send('success');
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

export default {
    getAccountInfo,
    putAccountInfo,
    postCreatePost,
    getAllPosts,
    patchStatusRented,
    getPostsUnappoved,
    getPostById,
    putEditPostById,
    getExpiredPosts,
    putExpiredPostById,
    getNotifications,
    patchNotification,
};
