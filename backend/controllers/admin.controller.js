import { User } from '../models/user.model.js';
import { Post } from '../models/post.model.js';
import { Notification } from '../models/notif.model.js';
import moment from 'moment';
import { Review } from '../models/review.model.js';
import { Report } from '../models/report.model.js';

// @controller GET unapprove accounts
// user/admin/un-accounts
// Lấy danh sách account chưa duyệt
const getUnapprovedAccount = async (req, res) => {
    try {
        const ownerList = await User.find({ is_approved: false });
        res.status(200).json(ownerList);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// @controller GET all accounts
// user/admin/accounts
// Lấy danh sách tất cả account
const getAllAccounts = async (req, res) => {
    try {
        const accounts = await User.find({ role: 'owner' });
        res.status(200).json(accounts);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// @controller PATCH unapprove account
// user/admin/accounts/:id
// Duyệt account
const patchApprovedAccount = async (req, res) => {
    // try {
    //     const id = req.params.id;
    //     const owner = await User.findByIdAndUpdate(id, { is_approved: true });
    //     res.status(200).json(owner);
    // } catch (err) {
    //     console.log(err);
    //     res.status(500).json({ success: false, message: err.message });
    // }
    try {
        const id = req.params.id;
        const owner = await User.findById(id);
        console.log(owner);
        if (owner.is_approved === true) {
            owner.is_approved = false;
        } else {
            owner.is_approved = true;
        }
        await owner.save();
        res.status(200).json(owner);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// @controller GET unapprove posts
// user/admin/un-posts
// Lấy danh sách bài post chưa duyệt
const getUnapprovedPost = async (req, res) => {
    try {
        const postList = await Post.find({ is_approved: false });
        res.status(200).json({ success: true, data: postList });
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// @controller GET unapprove post by id
// user/admin/un-posts/:id
// Lấy bài post chưa duyệt theo id
const getUnapprovedPostByID = async (req, res) => {
    try {
        const id = req.params.id;
        const post = await Post.findById(id).populate('user_id');
        res.status(200).json(post);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// @controller GET all posts
// user/admin/posts
// Lấy danh sách tất cả bài đăng
const getAllPosts = async (req, res) => {
    try {
        const postList = await Post.find();
        res.status(200).json(postList);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// @controller GET all posts
// user/admin/posts/:id
// Lấy danh sách tất cả bài đăng
const getPostById = async (req, res) => {
    const post_id = req.params.id;
    try {
        const post = await Post.findById(post_id).populate('user_id');
        res.status(200).json(post);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// @controller PATCH approve post
// user/admin/posts/:id
// Duyệt bài post
const patchApprovedPost = async (req, res) => {
    // const post_id = req.params.id;
    // try {
    //     const updatePost = await Post.findByIdAndUpdate(post_id, {
    //         is_approved: true,
    //         approve_date: moment().toISOString(),
    //     });
    //     res.status(200).json(updatePost);
    // } catch (err) {
    //     console.log(err);
    //     res.status(500).json({ message: err.message });
    // }

    const post_id = req.params.id;
    try {
        let post = await Post.findById(post_id);
        if (post.is_approved === true) {
            post.is_approved = false;
            post.approve_date = undefined;
        } else {
            post.is_approved = true;
            post.approve_date = moment().toISOString();
        }
        await post.save();

        const notification = {
            user_id: post.user_id,
            post_id: post_id,
            is_read: false,
            time: moment(),
            for: 'owner',
        };
        if (post.is_approved === true) {
            notification.title = 'Admin đã duyệt bài đăng ';
        } else {
            notification.title = 'Admin đã hủy duyệt bài đăng ';
        }

        const notificationForOwner = new Notification(notification);
        await notificationForOwner.save();

        res.status(200).json(post);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// @controller GET uneditable account
// user/admin/edit-auth
// Lấy danh sách tài khoản chưa cấp quyền chỉnh sửa
const getUnEditableAccount = async (req, res) => {
    try {
        const ownerList = await User.find({
            is_approved: true,
            editable: false,
        });
        res.status(200).json(ownerList);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// @controller PATCH editable account
// user/admin/edit-auth/:id
// Cấp quyền chỉnh sửa cho tài khoản
const patchEditableAccount = async (req, res) => {
    try {
        const id = req.params.id;
        const owner = await User.findById(id);
        console.log(owner);
        if (owner.editable === true) {
            owner.editable = false;
        } else {
            owner.editable = true;
        }
        await owner.save();
        res.status(200).json(owner);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

const getAllReports = async (req, res) => {
    try {
        const reports = await Report.find()
            .populate('user_id')
            .populate('post_id');
        res.status(200).json(reports);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// user/admin/reviews
const getAllReviews = async (req, res) => {
    try {
        const reviews = await Review.find()
            .populate('user_id')
            .populate('post_id');
        res.status(200).json(reviews);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// user/admin/reviews/:id
const patchReviewById = async (req, res) => {
    const id = req.params.id;
    try {
        const review = await Review.findByIdAndUpdate(id, {
            is_approved: true,
        });
        res.status(200).json(review);
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
    }
};

// user/admin/notif
const getNotifications = async (req, res) => {
    const user_id = req.signedCookies.user_id;

    try {
        const notifs = await Notification.find({
            for: 'admin',
            is_read: false,
        })
            .populate('user_id', 'name')
            .populate('post_id', 'title')
            .sort({ time: -1 });
        res.status(200).json(notifs);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

// user/admin/notif/:id
const patchNotification = async (req, res) => {
    const notif_id = req.params.id;
    try {
        await Notification.findByIdAndUpdate(notif_id, { is_read: true });
        res.status(200).send('success');
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: err.message });
    }
};

export default {
    getUnapprovedAccount,
    getAllAccounts,
    patchApprovedAccount,
    getUnapprovedPost,
    getAllPosts,
    getPostById,
    getUnapprovedPostByID,
    patchApprovedPost,
    getUnEditableAccount,
    patchEditableAccount,
    getAllReports,
    getAllReviews,
    patchReviewById,
    getNotifications,
    patchNotification,
};
