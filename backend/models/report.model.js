import mongoose from 'mongoose';

const ReportSchema = mongoose.Schema({
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User',
    },
    post_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Post',
    },
    report: {
        type: String,
    },
    time: {
        type: Date,
    },
});

export const Report = mongoose.model('Report', ReportSchema);
