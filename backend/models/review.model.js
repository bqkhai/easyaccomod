import mongoose from 'mongoose';

const ReviewSchema = mongoose.Schema({
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User',
    },
    post_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Post',
    },
    review: { type: String },
    star: { type: Number },
    is_approved: { type: Boolean },
    time: {
        type: Date,
    },
});

export const Review = mongoose.model('Review', ReviewSchema);
