import mongoose from 'mongoose';

const favouriteSchema = mongoose.Schema({
    user_id: mongoose.Schema.Types.ObjectId,
    post_list: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Post' }],
});

export const FavouritePost = mongoose.model('FavouritePost', favouriteSchema);
