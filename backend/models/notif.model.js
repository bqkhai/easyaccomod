import mongoose from 'mongoose';

const NotificationSchema = mongoose.Schema({
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User',
    },
    post_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Post',
    },
    is_read: {
        type: Boolean,
    },
    time: {
        type: Date,
    },
    for: {
        type: String,
        enum: ['admin', 'owner'],
    },
    title: {
        type: String,
    },
});

export const Notification = mongoose.model('Notification', NotificationSchema);
