import mongoose from 'mongoose';

const PostSchema = mongoose.Schema({
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    title: { type: String },
    // Địa chỉ
    city: { type: String },
    district: { type: String },
    ward: { type: String },
    street: { type: String },

    // Mô tả
    room_type: { type: String },
    room_price: { type: String },
    room_area: { type: String },
    image: [{ type: String }],

    // Tiện ích
    has_bathroom: { type: Boolean },
    has_kitchen: { type: Boolean },
    has_airConditioner: { type: Boolean },
    has_waterHeater: { type: Boolean },

    // Giá điện nước
    water_price: { type: String },
    electricity_price: { type: String },

    // Trạng thái
    rented_status: { type: Boolean },
    is_approved: { type: Boolean },
    approve_date: { type: Date },

    // Thời gian đăng bài
    time_post: { type: String },

    // Views + Like
    likes: { type: Number },
    views: { type: Number },
});

export const Post = mongoose.model('Post', PostSchema);
