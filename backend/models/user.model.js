import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
    role: {
        type: String,
        required: true,
        enum: ['admin', 'owner', 'renter'],
        // unique: false,
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    id_card_number: {
        type: String,
        // required: true,
        // unique: true,
    },
    address: {
        type: String,
        // required: true,
    },
    phone: {
        type: String,
        // required: true,
    },
    is_approved: {
        type: Boolean,
    },
    editable: {
        type: Boolean,
    },
});

export const User = mongoose.model('User', UserSchema);
