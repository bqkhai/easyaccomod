import React from 'react';
import { Link } from 'react-router-dom';
import LogOut from '../users/logout.component';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';

const username = localStorage.getItem('user');

const RenterNav = () => {
    return (
        <div className='sticky body-content'>
            <Navbar
                collapseOnSelect
                expand='lg'
                className='user-nav navbar-expand-sm'
                variant='dark'
            >
                <div className='container'>
                    <Navbar.Brand>
                        <Link
                            to='/user/renter'
                            className='navbar-brand text-light'
                        >
                            HouseForRent
                        </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                    <Navbar.Collapse id='responsive-navbar-brand text-light'>
                        <Nav className='mr-auto'>
                            <Link
                                to='/user/renter/favourite'
                                className='nav-link text-light'
                            >
                                Yêu thích
                            </Link>
                        </Nav>
                        <Nav>
                            <NavDropdown
                                title={username}
                                id='collasible-nav-dropdown'
                            >
                                <Link
                                    to='/user/change-password'
                                    className='nav-link text-dark'
                                >
                                    Đổi mật khẩu
                                </Link>

                                <LogOut />
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </div>
            </Navbar>
        </div>
    );
};

export default RenterNav;
