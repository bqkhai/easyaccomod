import React from 'react';
import { Link } from 'react-router-dom';
import LogOut from '../users/logout.component';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Footer from '../../components/footer/footer.component';

const username = localStorage.getItem('user');

const AdminNav = () => {
    return (
        <div className='sticky'>
            <Navbar
                collapseOnSelect
                expand='lg'
                className='user-nav navbar-expand-sm'
                variant='dark'
            >
                <div className='container'>
                    <Navbar.Brand>
                        <Link
                            to='/user/admin'
                            className='navbar-brand text-light'
                        >
                            HouseForRent
                        </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                    <Navbar.Collapse id='responsive-navbar-nav'>
                        <Nav className='mr-auto'>
                            <Link
                                to='/user/admin/notification'
                                className='nav-link text-light'
                            >
                                Thông báo
                            </Link>
                            <Link
                                to='/user/admin/posts'
                                className='nav-link text-light'
                            >
                                Bài đăng
                            </Link>
                            <Link
                                to='/user/admin/accounts'
                                className='nav-link text-light'
                            >
                                Tài khoản
                            </Link>
                            <Link
                                to='/user/admin/reports'
                                className='nav-link text-light'
                            >
                                Báo cáo
                            </Link>
                            <Link
                                to='/user/admin/reviews'
                                className='nav-link text-light'
                            >
                                Bình luận
                            </Link>
                            <Link
                                to='/user/admin/edit-auth'
                                className='nav-link text-light'
                            >
                                Cấp quyền chỉnh sửa
                            </Link>
                        </Nav>
                        <Nav>
                            <NavDropdown
                                title={username}
                                id='basic-nav-dropdown'
                            >
                                <Link
                                    to='/user/change-password'
                                    className='nav-link text-dark'
                                >
                                    Đổi mật khẩu
                                </Link>

                                <LogOut />
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </div>
            </Navbar>
            <Footer />
        </div>
    );
};

export default AdminNav;
