import React from 'react';
import { Link } from 'react-router-dom';
import LogOut from '../users/logout.component';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';

const username = localStorage.getItem('user');

const OwnerNav = () => {
    return (
        // <div className='sticky body-content'>
        <div className='sticky'>
            <Navbar
                collapseOnSelect
                expand='lg'
                className='user-nav navbar-expand-sm'
                variant='dark'
            >
                <div className='container'>
                    <Navbar.Brand>
                        <Link
                            to='/user/owner'
                            className='navbar-brand text-light'
                        >
                            HouseForRent
                        </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                    <Navbar.Collapse id='responsive-navbar-brand text-light'>
                        <Nav className='mr-auto'>
                            <Link
                                to='/user/owner/notification'
                                className='nav-link text-light'
                            >
                                Thông báo
                            </Link>
                            <Link
                                to='/user/owner/posts'
                                className='nav-link text-light'
                            >
                                Bài đăng
                            </Link>
                            <Link
                                to='/user/owner/extend'
                                className='nav-link text-light'
                            >
                                Gia hạn
                            </Link>
                        </Nav>
                        <Nav>
                            <NavDropdown
                                title={username}
                                id='collasible-nav-dropdown'
                            >
                                <Link
                                    to='/user/change-password'
                                    className='nav-link text-dark'
                                >
                                    Đổi mật khẩu
                                </Link>

                                <Link
                                    to='/user/change-info'
                                    className='nav-link text-dark'
                                >
                                    Thay đổi thông tin
                                </Link>

                                <LogOut />
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </div>
            </Navbar>
        </div>
    );
};

export default OwnerNav;
