import React from 'react';
import { Link } from 'react-router-dom';
import { MDBFooter, MDBIcon } from 'mdb-react-ui-kit';

import './footer.css';
const user_role = localStorage.getItem('user_role');
const Footer = () => {
    return (
        <MDBFooter className='bg-light text-center text-white footer-box'>
            <div className='container p-0 pb-0'>
                <section className='mb-1'>
                    <a
                        className='btn btn-primary btn-floating m-1'
                        style={{ backgroundColor: '#3b5998' }}
                        href='https://fb.com/buiquangkhai297'
                        role='button'
                    >
                        <MDBIcon fab icon='facebook-f' />
                    </a>

                    <a
                        className='btn btn-primary btn-floating m-1'
                        style={{ backgroundColor: '#dd4b39' }}
                        href='mailto:quangkhai123unet@gmail.com'
                        role='button'
                    >
                        <MDBIcon fab icon='google' />
                    </a>
                    <a
                        className='btn btn-primary btn-floating m-1'
                        style={{ backgroundColor: '#ac2bac' }}
                        href='https://instagram.com'
                        role='button'
                    >
                        <MDBIcon fab icon='instagram' />
                    </a>

                    <a
                        className='btn btn-primary btn-floating m-1'
                        style={{ backgroundColor: '#0082ca' }}
                        href='https://linkedin.com'
                        role='button'
                    >
                        <MDBIcon fab icon='linkedin-in' />
                    </a>

                    <a
                        className='btn btn-primary btn-floating m-1'
                        style={{ backgroundColor: '#333333' }}
                        href='https://github.com/bqkhai'
                        role='button'
                    >
                        <MDBIcon fab icon='github' />
                    </a>
                </section>
            </div>

            <div className='text-center text-black-50'>
                © 2022 Copyright:
                {/* <a
                    className='text-white'
                    href=`${process.env.REACT_APP_FE_URL}/user/owner`
                >
                    HouseForRent
                </a> */}
                <Link to={`/user/${user_role}`} className=''>
                    {' '}
                    HouseForRent ~ Khai Bui Quang
                </Link>
            </div>
        </MDBFooter>
    );
};

export default Footer;
