import React, { useEffect, useState } from 'react';
import axios from 'axios';
import AdminNav from '../common/admin_nav.component';
import { Link } from 'react-router-dom';

import dotenv from 'dotenv';
dotenv.config();

const ManagePosts = () => {
    const [data, setData] = useState([]);

    const loadData = () => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/admin/posts`, {
                withCredentials: true,
            })
            .then((res) => {
                console.log(res.data);
                setData(res.data);
            })
            .catch((err) => {
                if (err.response.status === 401) {
                    alert('Bạn không có quyền làm điều này');
                }
            });
    };

    useEffect(() => {
        loadData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const approvePost = (id) => {
        axios
            .patch(
                `${process.env.REACT_APP_HOST_URL}/user/admin/posts/${id}`,
                {},
                {
                    withCredentials: true,
                }
            )
            .then((res) => {
                loadData();
                console.log(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const postList = () => {
        return data.map((post, index) => {
            return (
                <tr key={index}>
                    <td id='title'>{post.title}</td>
                    <td id='address'>{post.city}</td>
                    <td id='street'>{post.street}</td>
                    <td id='area'>{post.room_area}</td>
                    <td id='view'>
                        {post.views} / {post.likes}
                    </td>
                    <td id='time_post'>{post.time_post}</td>
                    <td
                        id='status'
                        onClick={() => {
                            approvePost(post._id);
                        }}
                    >
                        {post.is_approved ? (
                            <p className='text-success'>Đã duyệt</p>
                        ) : (
                            <p className='text-danger'>Chưa duyệt</p>
                        )}
                    </td>
                    <td id='detail'>
                        <Link to={`/user/admin/posts/${post._id}`}>Xem</Link>
                    </td>
                </tr>
            );
        });
    };

    return (
        <div>
            <AdminNav />
            <div className='container'>
                <br />
                <div className='row'>
                    <h3 className='mr-auto'>Danh sách bài đăng</h3>
                </div>
                <br />
                <br />
                <table className='table table-hover'>
                    <thead className='thead-light'>
                        <tr>
                            <th scope='col'>Bài đăng</th>
                            <th scope='col'>Địa chỉ</th>
                            <th scope='col'>Đường</th>
                            <th scope='col'>Diện tích</th>
                            <th scope='col'>Views/Likes</th>
                            <th scope='col'>Thời gian</th>
                            <th scope='col'>Tình trạng</th>
                            <th scope='col'>Chi tiết</th>
                        </tr>
                    </thead>
                    <tbody>{postList()}</tbody>
                </table>
            </div>
        </div>
    );
};

export default ManagePosts;
