import React, { useEffect, useState } from 'react';
import axios from 'axios';
import moment from 'moment';
import AdminNav from '../common/admin_nav.component';

import dotenv from 'dotenv';
dotenv.config();

const ManageReports = () => {
    const [data, setData] = useState([]);

    const loadData = () => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/admin/reports`, {
                withCredentials: true,
            })
            .then((res) => {
                console.log(res.data);
                setData(res.data);
            })
            .catch((err) => {
                if (err.response.status === 401) {
                    alert('Bạn không có quyền làm điều này');
                }
            });
    };

    useEffect(() => {
        loadData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const reportList = () => {
        return data.map((report, index) => {
            return (
                <tr key={index}>
                    <td>{report.user_id.name}</td>
                    <td>{report.post_id.title}</td>
                    <td>{report.report}</td>
                    <td>
                        {moment(report.time).format('MMMM Do YYYY, h:mm:ss a')}
                    </td>
                </tr>
            );
        });
    };

    return (
        <div>
            <AdminNav />
            <div className='container'>
                <br />
                <div className='row'>
                    <h3 className='mr-auto'>Danh sách báo cáo</h3>
                </div>
                <br />
                <br />
                <table className='table table-hover'>
                    <thead className='thead-light'>
                        <tr>
                            <th scope='col'>Người gửi</th>
                            <th scope='col'>Bài đăng</th>
                            <th scope='col'>Nội dung</th>
                            <th scope='col'>Thời gian</th>
                        </tr>
                    </thead>
                    <tbody>{reportList()}</tbody>
                </table>
            </div>
        </div>
    );
};

export default ManageReports;
