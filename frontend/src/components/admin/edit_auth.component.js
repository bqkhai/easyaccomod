import React, { useEffect, useState } from 'react';
import axios from 'axios';
import AdminNav from '../common/admin_nav.component';

import dotenv from 'dotenv';
dotenv.config();

const ManagerEditAuth = () => {
    const [data, setData] = useState([]);

    const loadData = () => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/admin/accounts`, {
                withCredentials: true,
            })
            .then((res) => {
                // console.log(res.data);
                setData(res.data);
            })
            .catch((err) => {
                if (err.response.status === 401) {
                    alert('Bạn không có quyền làm điều này');
                }
                // console.log(err.response);
            });
    };

    useEffect(() => {
        loadData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const changePermission = (id) => {
        axios
            .patch(
                `${process.env.REACT_APP_HOST_URL}/user/admin/edit-auth/${id}`,
                {},
                {
                    withCredentials: true,
                }
            )
            .then((res) => {
                loadData();
                console.log(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const accountList = () => {
        return data.map((account, index) => {
            return (
                <tr key={index}>
                    <td id='name'>{account.name}</td>
                    <td id='email'>{account.email}</td>
                    <td id='card'>{account.id_card_number}</td>
                    <td id='address'>{account.address}</td>
                    <td id='phone'>{account.phone}</td>

                    <td
                        onClick={() => {
                            changePermission(account._id);
                        }}
                    >
                        {account.editable ? (
                            <p className='text-success'>Cho phép</p>
                        ) : (
                            <p className='text-danger'>Không cho phép</p>
                        )}
                    </td>
                </tr>
            );
        });
    };

    return (
        <div>
            <AdminNav />
            <div className='container'>
                <br />
                <div className='row'>
                    <h3 className='mr-auto'>Danh sách tài khoản</h3>
                </div>
                <br />
                <br />
                <table className='table table-hover'>
                    <thead className='thead-light'>
                        <tr>
                            <th scope='col'>Tên tài khoản</th>
                            <th scope='col'>Email</th>
                            <th scope='col'>CMT/ CCCD</th>
                            <th scope='col'>Địa chỉ</th>
                            <th scope='col'>Số điện thoại</th>
                            <th scope='col'>Tình trạng</th>
                        </tr>
                    </thead>
                    <tbody>{accountList()}</tbody>
                </table>
            </div>
        </div>
    );
};

export default ManagerEditAuth;
