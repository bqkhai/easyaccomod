import React, { useEffect, useState } from 'react';
import axios from 'axios';
import moment from 'moment';
import AdminNav from '../common/admin_nav.component';

import dotenv from 'dotenv';
dotenv.config();

const ManageReviews = () => {
    const [data, setData] = useState([]);

    const loadData = () => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/admin/reviews`, {
                withCredentials: true,
            })
            .then((res) => {
                console.log(res.data);
                setData(res.data);
            })
            .catch((err) => {
                if (err.response.status === 401) {
                    alert('Bạn không có quyền làm điều này');
                }
            });
    };

    useEffect(() => {
        loadData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const approvedReview = (id) => {
        axios
            .patch(
                `${process.env.REACT_APP_HOST_URL}/user/admin/reviews/${id}`,
                {},
                {
                    withCredentials: true,
                }
            )
            .then((res) => {
                loadData();
                console.log(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const reviewList = () => {
        return data.map((review, index) => {
            return (
                <tr key={index}>
                    <td>{review.user_id.name}</td>
                    <td>{review.post_id.title}</td>
                    <td id='review'>{review.review}</td>
                    <td>{review.star}</td>
                    <td>
                        {moment(review.time).format('MMMM Do YYYY, h:mm:ss a')}
                    </td>
                    <td
                        onClick={() => {
                            approvedReview(review._id);
                        }}
                    >
                        {review.is_approved ? (
                            <p className='text-success'>Đã duyệt</p>
                        ) : (
                            <p className='text-danger'>Chưa duyệt</p>
                        )}
                    </td>
                </tr>
            );
        });
    };

    return (
        <div>
            <AdminNav />
            <div className='container'>
                <br />
                <div className='row'>
                    <h3 className='mr-auto'>Danh sách comment</h3>
                </div>
                <br />
                <br />
                <table className='table table-hover'>
                    <thead className='thead-light'>
                        <tr>
                            <th scope='col'>Người gửi</th>
                            <th scope='col'>Bài đăng</th>
                            <th scope='col'>Nội dung</th>
                            <th scope='col'>Rating</th>
                            <th scope='col'>Thời gian</th>
                            <th scope='col'>Phê duyệt</th>
                        </tr>
                    </thead>
                    <tbody>{reviewList()}</tbody>
                </table>
            </div>
        </div>
    );
};

export default ManageReviews;
