import React, { useEffect, useState } from 'react';
import axios from 'axios';
import AdminNav from '../common/admin_nav.component';
import { useParams } from 'react-router-dom';
import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/styles.css';
import withAutoplay from 'react-awesome-slider/dist/autoplay';

import dotenv from 'dotenv';

const AutoplaySlider = withAutoplay(AwesomeSlider);
dotenv.config();

const PostDetail = () => {
    const [data, setData] = useState({
        id: '',
        title: '',
        city: '',
        district: '',
        ward: '',
        street: '',
        room_type: '',
        room_price: '',
        room_area: '',
        has_bathroom: '',
        has_kitchen: '',
        has_airConditioner: '',
        has_waterHeater: '',
        electricity_price: '',
        water_price: '',
        image: [],
        time_post: '',

        owner_name: '',
        owner_phone: '',
        is_approved: '',
    });

    const { id } = useParams();

    const loadData = () => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/admin/posts/${id}`, {
                withCredentials: true,
            })
            .then((res) => {
                console.log(res.data);
                setData({
                    id: res.data.id,
                    title: res.data.title,
                    city: res.data.city,
                    district: res.data.district,
                    ward: res.data.ward,
                    street: res.data.street,
                    room_type: res.data.room_type,
                    room_price: res.data.room_price,
                    room_area: res.data.room_area,
                    has_bathroom: res.data.has_bathroom,
                    has_kitchen: res.data.has_kitchen,
                    has_airConditioner: res.data.has_airConditioner,
                    has_waterHeater: res.data.has_waterHeater,
                    electricity_price: res.data.electricity_price,
                    water_price: res.data.water_price,
                    image: res.data.image,
                    time_post: res.data.time_post,
                    is_approved: res.data.is_approved,
                    owner_name: res.data.user_id.name,
                    owner_phone: res.data.user_id.phone,
                });
                console.log(data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        console.log(id);
        loadData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onClickApproved = () => {
        axios
            .patch(
                `${process.env.REACT_APP_HOST_URL}/user/admin/posts/${id}`,
                {},
                {
                    withCredentials: true,
                }
            )
            .then((res) => {
                console.log(res.data);
                setData(res.data);
                loadData();
                // window.location.reload();
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div>
            <AdminNav />
            <div className='container'>
                <br />
                {/* <h3>Chi tiết phòng</h3> */}
                <br />
                <div className='row'>
                    <div className='col-sm-8 image'>
                        <AutoplaySlider
                            play={true}
                            cancelOnInteraction={false}
                            interval={1000}
                        >
                            {data.image.map((img, index) => (
                                <div key={index} className='yours-custom-class'>
                                    <img
                                        src={img}
                                        width='100%'
                                        alt='room_image'
                                    ></img>
                                </div>
                            ))}
                        </AutoplaySlider>
                    </div>
                    <div className='col-sm-4 text-box'>
                        <h2 className='text-capitalize'>{data.title}</h2>
                        <p className='rented-rate'>
                            {data.room_price} VNĐ/Tháng
                        </p>
                        <br />
                        <div>
                            {/* <h5>Trạng thái</h5> */}
                            {data.rented_status ? (
                                <h5 className='text-danger'>
                                    Đã có người thuê
                                </h5>
                            ) : (
                                <h5 className='text-success'>
                                    Chưa có người thuê
                                </h5>
                            )}
                        </div>
                        <br />
                        {!data.is_approved ? (
                            <button
                                className='btn btn-success'
                                onClick={() => onClickApproved()}
                            >
                                Duyệt
                            </button>
                        ) : (
                            <button
                                className='btn btn-danger'
                                onClick={() => onClickApproved()}
                            >
                                Bỏ Duyệt
                            </button>
                        )}
                    </div>
                </div>
                <br />
                <br />
                <div className='row'>
                    <div className='col-sm-6'>
                        <h2>Thông tin chung</h2>
                        <br />
                        <div className='form-row'>
                            <i className='fas fa-map-marked-alt'></i>
                            <p>
                                Địa chỉ: {data.street}, {data.ward},{' '}
                                {data.district}, {data.city}
                            </p>
                        </div>
                        <div className='form-row'>
                            <i className='fas fa-home'></i>
                            <p>Loại phòng: {data.room_type}</p>
                        </div>
                        <div className='form-row'>
                            <i className='fas fa-warehouse'></i>
                            <p>Diện tích: {data.area} M2</p>
                        </div>
                        <div className='form-row'>
                            <i className='fas fa-couch'></i>
                            <p>
                                Tiện ích:{' '}
                                {data.has_bathroom ? (
                                    <span>Phòng tắm</span>
                                ) : (
                                    <span></span>
                                )}{' '}
                                {data.has_kitchen ? (
                                    <span>, Phòng bếp</span>
                                ) : (
                                    <span></span>
                                )}{' '}
                                {data.has_airConditioner ? (
                                    <span>, Điều hòa</span>
                                ) : (
                                    <span></span>
                                )}{' '}
                                {data.has_waterHeater ? (
                                    <span>, Nóng lạnh</span>
                                ) : (
                                    <span></span>
                                )}
                            </p>
                        </div>
                        <div className='form-row'>
                            <i className='fas fa-credit-card'></i>
                            <p>
                                Dịch vụ: Giá điện: {data.electricity}
                                VNĐ/KWH, Giá nước: {data.water}VNĐ/M3
                            </p>
                        </div>
                    </div>
                    <div className='col-sm-6'>
                        <h2>Thông tin chủ trọ</h2>
                        <br />
                        <div className='form-row'>
                            <i className='fas fa-user-tag'></i>
                            <p>Họ Tên: {data.owner_name}</p>
                        </div>
                        <div className='form-row'>
                            <i className='fas fa-phone'></i>
                            <p>Số điện thoại: {data.owner_phone}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PostDetail;
