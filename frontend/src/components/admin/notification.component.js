import React, { useEffect, useState } from 'react';
import axios from 'axios';
import AdminNav from '../common/admin_nav.component';
import moment from 'moment';

import dotenv from 'dotenv';
dotenv.config();

const Notification = () => {
    const [data, setData] = useState([]);

    const loadData = () => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/admin/notif`, {
                withCredentials: true,
            })
            .then((res) => {
                console.log(res.data);
                setData(res.data);
            })
            .catch((err) => {
                if (err.response.status === 401) {
                    alert('Bạn không có quyền làm điều này');
                }
            });
    };

    useEffect(() => {
        loadData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const delNotif = (id) => {
        axios
            .patch(
                `${process.env.REACT_APP_HOST_URL}/user/admin/notif/${id}`,
                {},
                {
                    withCredentials: true,
                }
            )
            .then((res) => {
                loadData();
                console.log(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const postList = () => {
        return data.map((notif, index) => {
            return (
                <tr key={index}>
                    <td id='title-notif-admin'>
                        Bài đăng{' '}
                        <span className='text-info'>{notif.post_id.title}</span>
                        {' của chủ trọ: '}
                        <span className='text-primary'>
                            {notif.user_id.name}{' '}
                        </span>
                        {'đã thay đổi trạng thái thành'}
                        {notif.title === ' đã có người thuê' ? (
                            <span className='text-danger'>{notif.title}</span>
                        ) : (
                            <span className='text-success'>{notif.title}</span>
                        )}
                    </td>
                    <td>
                        {moment(notif.time).format('MMMM Do YYYY, h:mm:ss a')}
                    </td>
                    <td
                        onClick={() => {
                            delNotif(notif._id);
                        }}
                        className='text-danger'
                        id='notif-del'
                    >
                        Xóa
                    </td>
                </tr>
            );
        });
    };

    return (
        <div>
            <AdminNav />
            <div className='container'>
                <br />
                <div className='row'>
                    <h3 className='mr-auto'>Thông báo</h3>
                </div>
                <br />
                <br />
                <table className='table table-hover'>
                    <tbody>{postList()}</tbody>
                </table>
            </div>
        </div>
    );
};

export default Notification;
