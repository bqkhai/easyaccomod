import React from 'react';
import AdminNav from '../../components/common/admin_nav.component';
import AllPosts from '../admin/posts.component';

const AdminWS = () => {
    return (
        <div>
            <AllPosts />
        </div>
    );
};

export default AdminWS;
