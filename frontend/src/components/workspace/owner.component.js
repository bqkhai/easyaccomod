import React from 'react';
import OwnerNav from '../../components/common/owner_nav.component';
import AllPosts from '../../components/owner/all_posts.component';
import Footer from '../footer/footer.component';

const OwnerWS = () => {
    return (
        <div>
            <AllPosts />
            <Footer />
        </div>
    );
};

export default OwnerWS;
