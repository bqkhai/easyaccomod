import React from 'react';
import RenterNav from '../../components/common/renter_nav.component';
import Posts from '../../components/renter/list_posts.component';
import Search from '../../components/renter/search.component';
import Footer from '../../components/footer/footer.component';

const RenterWS = () => {
    return (
        <div>
            <RenterNav />
            <Search />
        </div>
    );
};

export default RenterWS;
