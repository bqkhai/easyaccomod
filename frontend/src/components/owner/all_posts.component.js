import React, { useEffect, useState } from 'react';
import axios from 'axios';
import OwnerNav from '../common/owner_nav.component';

import dotenv from 'dotenv';
import { Link } from 'react-router-dom';
import Footer from '../footer/footer.component';
dotenv.config();
const baseUrl = process.env.REACT_APP_HOST_URL;

const AllPosts = () => {
    const [data, setData] = useState([]);

    const loadData = () => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/owner/posts`, {
                withCredentials: true,
            })
            .then((res) => {
                // console.log(res.status);
                setData(res.data);
            })
            .catch((err) => {
                if (err.response.status === 401) {
                    console.log(err.response.data);
                    alert('Bạn không có quyền làm điều này');
                } else {
                    console.log(err);
                }
            });
    };

    useEffect(() => {
        loadData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const changeStatus = (id) => {
        axios
            .patch(
                `${baseUrl}/user/owner/status-rented/${id}`,
                {},
                {
                    withCredentials: true,
                }
            )
            .then((res) => {
                loadData();
                console.log(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const postList = () => {
        return data.map((post, index) => {
            return (
                <tr key={index}>
                    <td>{post.title}</td>
                    <td>{post.city}</td>
                    <td>{post.street}</td>
                    <td>{post.views}</td>
                    <td>
                        {post.is_approved ? (
                            <p className='text-primary'>Đúng</p>
                        ) : (
                            <p className='text-danger'>Sai</p>
                        )}
                    </td>
                    <td
                        onClick={() => {
                            changeStatus(post._id);
                            // post.rented_status = !post.rented_status;
                        }}
                    >
                        {post.rented_status ? (
                            <p className='text-danger'>Đã thuê</p>
                        ) : (
                            <p className='text-success'>Chưa thuê</p>
                        )}
                    </td>
                    <td>
                        <Link to={`/user/owner/edit/${post._id}`}>
                            Chỉnh sửa
                        </Link>
                    </td>
                </tr>
            );
        });
    };

    return (
        <div className='content'>
            <OwnerNav />
            <div className='container'>
                <br />
                <div className='row'>
                    <h3 className='mr-auto'>Danh sách bài đăng</h3>
                    <Link to='/user/owner/post' className='add-btn'>
                        <button className='btn btn-add btn-info'>
                            Thêm mới
                        </button>
                    </Link>
                </div>
                <br />
                <br />
                <table className='table table-hover'>
                    <thead className='thead-light'>
                        <tr>
                            <th scope='col'>Tiêu đề</th>
                            <th scope='col'>Thành phố</th>
                            <th scope='col'>Đường</th>
                            <th scope='col'>Số views</th>
                            <th scope='col'>Đã duyệt</th>
                            <th scope='col'>Tình trạng</th>
                            <th scope='col'>Hành động</th>
                        </tr>
                    </thead>
                    <tbody>{postList()}</tbody>
                </table>
            </div>
            <Footer />
        </div>
    );
};

export default AllPosts;
