import React, { useEffect, useState } from 'react';
import axios from 'axios';
import OwnerNav from '../common/owner_nav.component';
import moment from 'moment';

import dotenv from 'dotenv';
import Footer from '../footer/footer.component';
dotenv.config();

const Notification = () => {
    // const [data, setData] = useState([]);
    // useEffect(() => {
    //     axios
    //         .get(`${process.env.REACT_APP_HOST_URL}/user/owner/notif`, {
    //             withCredentials: true,
    //         })
    //         .then((res) => {
    //             console.log(res.data);
    //             // debugger;
    //             setData(res.data);
    //         })
    //         .catch((err) => {
    //             console.log(err);
    //         });
    // }, []);
    // const clearNotif = (id) => {
    //     axios
    //         .patch(
    //             `${process.env.REACT_APP_HOST_URL}/user/owner/notif/${id}`,
    //             {},
    //             { withCredentials: true }
    //         )
    //         .then((res) => {
    //             console.log(res.data);
    //         })
    //         .catch((err) => console.log(err));
    //     // setData(data.filter((notif) => notif._id !== id));
    // };
    // const NotifsList = () => {
    //     return data.map((notification, index) => {
    //         return (
    //             <tr className='bg-light' key={index}>
    //                 <td>
    //                     Bài đăng của bạn{' '}
    //                     <span className='text-success'>
    //                         {/* {notification.post_id.title} */}
    //                     </span>{' '}
    //                     đã được phê duyệt
    //                 </td>
    //                 <td id='right-text'>
    //                     <a
    //                         href='#/'
    //                         className='text-danger'
    //                         onClick={clearNotif(notification._id)}
    //                     >
    //                         Xóa
    //                     </a>
    //                 </td>
    //             </tr>
    //         );
    //     });
    // };
    // return (
    //     <div>
    //         <OwnerNav />
    //         <br />
    //         <div className='container'>
    //             <table className='table table-hover'>
    //                 <tbody>{NotifsList()}</tbody>
    //             </table>
    //         </div>
    //     </div>
    // );

    const [data, setData] = useState([]);

    const loadData = () => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/owner/notif`, {
                withCredentials: true,
            })
            .then((res) => {
                console.log(res.data);
                setData(res.data);
            })
            .catch((err) => {
                if (err.response.status === 401) {
                    console.log(err.response.data);
                    alert('Bạn không có quyền làm điều này');
                } else {
                    console.log(err);
                }
            });
    };

    useEffect(() => {
        loadData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const delNotif = (id) => {
        axios
            .patch(
                `${process.env.REACT_APP_HOST_URL}/user/owner/notif/${id}`,
                {},
                {
                    withCredentials: true,
                }
            )
            .then((res) => {
                loadData();
                console.log(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const postList = () => {
        return data.map((notif, index) => {
            return (
                <tr key={index}>
                    <td id='title-notif-owner'>
                        {notif.title === 'Admin đã hủy duyệt bài đăng ' ? (
                            <span className='text-danger'>{notif.title}</span>
                        ) : (
                            <span className='text-success'>{notif.title}</span>
                        )}
                        <span className='text-info'>{notif.post_id.title}</span>{' '}
                        của bạn
                    </td>
                    <td>
                        {moment(notif.time).format('MMMM Do YYYY, h:mm:ss a')}
                    </td>
                    <td
                        onClick={() => {
                            delNotif(notif._id);
                        }}
                        className='text-danger'
                        id='notif-del'
                    >
                        Xóa
                    </td>
                </tr>
            );
        });
    };

    return (
        <div className='content'>
            <OwnerNav />
            <div className='container'>
                <br />
                <div className='row'>
                    <h3 className='mr-auto'>Thông báo</h3>
                </div>

                <table className='table table-hover notif'>
                    <tbody>{postList()}</tbody>
                </table>
            </div>
            <Footer />
        </div>
    );
};

export default Notification;
