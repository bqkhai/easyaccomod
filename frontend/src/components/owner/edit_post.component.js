import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import OwnerNav from '../common/owner_nav.component';
import Footer from '../../components/footer/footer.component';

import dotenv from 'dotenv';
dotenv.config();

const EditPost = () => {
    const [data, setData] = useState({
        title: '',
        city: '',
        district: '',
        ward: '',
        street: '',
        room_type: '',
        room_price: '',
        room_area: '',
        has_bathroom: '',
        has_kitchen: '',
        has_airConditioner: '',
        has_waterHeater: '',
        electricity_price: '',
        water_price: '',
        time_post: '',
        errors: {},
    });

    const { id } = useParams();

    useEffect(() => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/owner/edit/${id}`, {
                withCredentials: true,
            })
            .then((res) => {
                console.log(res.data);
                setData({
                    ...res.data,
                    errors: {},
                });
            })
            .catch((err) => {
                console.log(err);
            });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleChange = (e) => {
        const value =
            e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        setData({
            ...data,
            [e.target.name]: value,
        });
        // console.log(data);
    };

    const validation = () => {
        let errors = {};
        let formValidation = true;

        const string_re =
            /^[0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
        const area_re = /^[1-9][0-9]*[,.]{0,1}[0-9]*$/;
        const money_re = /^[1-9][0-9]*$/;
        const time_post_re = /^[1-4]/;

        if (!data.title) {
            formValidation = false;
            errors.title = 'Không được bỏ trống';
        }
        if (data.title && !data.title.match(string_re)) {
            formValidation = false;
            errors.title = 'Trường này không đúng định dạng';
        }

        if (!data.city) {
            formValidation = false;
            errors.city = 'Không được bỏ trống';
        }
        if (data.city && !data.city.match(string_re)) {
            formValidation = false;
            errors.city = 'Trường này không đúng định dạng';
        }

        if (!data.district) {
            formValidation = false;
            errors.district = 'Không được bỏ trống';
        }
        if (data.district && !data.district.match(string_re)) {
            formValidation = false;
            errors.district = 'Trường này không đúng định dạng';
        }

        if (!data.ward) {
            formValidation = false;
            errors.ward = 'Không được bỏ trống';
        }
        if (data.ward && !data.ward.match(string_re)) {
            formValidation = false;
            errors.ward = 'Trường này không đúng định dạng';
        }

        if (!data.street) {
            formValidation = false;
            errors.street = 'Không được bỏ trống';
        }
        if (data.street && !data.street.match(string_re)) {
            formValidation = false;
            errors.street = 'Trường này không đúng định dạng';
        }

        if (!data.room_type) {
            formValidation = false;
            errors.room_type = 'Không được bỏ trống';
        }
        if (
            data.room_type &&
            data.room_type !== 'Nhà nguyên căn' &&
            data.room_type !== 'Chung cư mini' &&
            data.room_type !== 'Chung cư' &&
            data.room_type !== 'Phòng trọ' &&
            data.room_type !== 'Nhà trọ giá rẻ'
        ) {
            formValidation = false;
            errors.room_type = 'Dữ liệu sai định dạng';
        }

        if (!data.room_price) {
            formValidation = false;
            errors.room_price = 'Không được bỏ trống';
        }
        if (data.room_price && !data.room_price.match(money_re)) {
            formValidation = false;
            errors.room_price = 'Giá thuê cần là số';
        }

        if (!data.room_area) {
            formValidation = false;
            errors.room_area = 'Không được bỏ trống';
        }
        if (data.room_area && !data.room_area.match(area_re)) {
            formValidation = false;
            errors.room_area = 'Diện tích là số';
        }

        if (!data.electricity_price) {
            formValidation = false;
            errors.electricity_price = 'Không được bỏ trống';
        }
        if (data.electricity_price && !data.electricity_price.match(money_re)) {
            formValidation = false;
            errors.electricity_price = 'Giá điện cần là số';
        }

        if (!data.water_price) {
            formValidation = false;
            errors.water_price = 'Không được bỏ trống';
        }
        if (data.water_price && !data.water_price.match(money_re)) {
            formValidation = false;
            errors.water_price = 'Giá nước cần là số';
        }

        if (!data.time_post) {
            formValidation = false;
            errors.time_post = 'Không được bỏ trống';
        }
        if (data.time_post && !data.time_post.match(time_post_re)) {
            formValidation = false;
            errors.time_post = 'Số tuần đăng từ 1-4';
        }

        setData({
            ...data,
            errors: errors,
        });
        return formValidation;
    };

    const onSubmit = (e) => {
        e.preventDefault();

        const editPostData = {
            ...data,
        };

        console.log(editPostData);

        if (validation()) {
            axios
                .put(
                    `${process.env.REACT_APP_HOST_URL}/user/owner/edit/${id}`,
                    editPostData,
                    {
                        withCredentials: true,
                    }
                )
                .then((res) => {
                    if (res.status === 200) {
                        alert('Thành công! Hãy chờ đợi admin phê duyệt');
                        window.location.href = `${process.env.REACT_APP_FE_URL}/user/owner/posts`;
                    }
                })
                .catch((err) => {
                    if (err.response.status === 401) {
                        console.log(err.response.data);
                        alert('Bạn không có quyền làm điều này');
                    } else {
                        console.log(err);
                    }
                });
        }
    };

    return (
        <div className='content'>
            <OwnerNav />
            <div className='container  container-box'>
                <form
                    action=''
                    method='PUT'
                    onSubmit={onSubmit}
                    encType='multipart/form-data'
                >
                    <div>
                        <br />
                        <h5>Tiêu đề</h5>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='title'
                                type='text'
                                name='title'
                                value={data.title}
                                onChange={handleChange}
                                placeholder='Tiêu đề'
                            ></input>
                            <span style={{ color: 'red' }}>
                                {data.errors.title}
                            </span>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-4'>
                            <br />
                            <h5>Địa chỉ</h5>
                            <div className='form-group'>
                                <label htmlFor='city'>Thành phố</label>
                                <input
                                    className='form-control'
                                    id='city'
                                    type='text'
                                    name='city'
                                    value={data.city}
                                    onChange={handleChange}
                                    placeholder='Thành phố'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.city}
                                </span>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='district'>Quận</label>
                                <input
                                    className='form-control'
                                    id='district'
                                    type='text'
                                    name='district'
                                    value={data.district}
                                    onChange={handleChange}
                                    placeholder='Quận'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.district}
                                </span>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='ward'>Phường</label>
                                <input
                                    className='form-control'
                                    id='ward'
                                    type='text'
                                    name='ward'
                                    value={data.ward}
                                    onChange={handleChange}
                                    placeholder='Phường'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.ward}
                                </span>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='street'>Đường</label>
                                <input
                                    className='form-control'
                                    id='street'
                                    type='text'
                                    name='street'
                                    value={data.street}
                                    onChange={handleChange}
                                    placeholder='Đường'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.street}
                                </span>
                            </div>
                        </div>
                        <div className='col-sm-4'>
                            <h5>Mô tả phòng</h5>
                            <div className='form-group'>
                                <label htmlFor='room_type'>Loại hình</label>
                                <select
                                    className='form-control'
                                    id='room_type'
                                    type='text'
                                    name='room_type'
                                    value={data.room_type}
                                    onChange={handleChange}
                                >
                                    <option>Nhà nguyên căn</option>
                                    <option>Chung cư mini</option>
                                    <option>Chung cư</option>
                                    <option>Phòng trọ</option>
                                    <option>Nhà trọ giá rẻ</option>
                                </select>
                                <span style={{ color: 'red' }}>
                                    {data.errors.room_type}
                                </span>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='room_area'>
                                    Diện tích (m2)
                                </label>
                                <input
                                    className='form-control'
                                    id='room_area'
                                    type='text'
                                    name='room_area'
                                    value={data.room_area}
                                    onChange={handleChange}
                                    placeholder='Mét vuông'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.room_area}
                                </span>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='room_price'>
                                    Giá nguyên căn hoặc giá thuê trên tháng
                                    (vnđ)
                                </label>
                                <input
                                    className='form-control'
                                    id='room_price'
                                    type='text'
                                    name='room_price'
                                    value={data.room_price}
                                    onChange={handleChange}
                                    placeholder='VNĐ'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.room_price}
                                </span>
                            </div>
                        </div>
                        <div className='col-sm-4'>
                            <h5>Giá cả</h5>
                            <div className='form-group'>
                                <label htmlFor='electricity_price'>
                                    Giá điện một số (vnđ)
                                </label>
                                <input
                                    className='form-control'
                                    id='electricity_price'
                                    type='text'
                                    name='electricity_price'
                                    value={data.electricity_price}
                                    onChange={handleChange}
                                    placeholder='VNĐ/kWh'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.electricity_price}
                                </span>
                            </div>
                            <div className='form-group'>
                                <label htmlFor='electricity_price'>
                                    Giá nước một khối (vnđ)
                                </label>
                                <input
                                    className='form-control'
                                    id='water_price'
                                    type='text'
                                    name='water_price'
                                    value={data.water_price}
                                    onChange={handleChange}
                                    placeholder='VNĐ/khối'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.water_price}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-4'>
                            <br />
                            <h5>Tiện ích</h5>
                            <div className='row'>
                                <div className='col-sm-6'>
                                    {data.has_bathroom ? (
                                        <div className='form-group form-check'>
                                            <input
                                                className='form-check-input'
                                                id='bathroom'
                                                type='checkbox'
                                                name='bathroom'
                                                value={data.has_bathroom}
                                                onChange={handleChange}
                                                defaultChecked
                                            />
                                            <label htmlFor='bathroom'>
                                                Nhà tắm
                                            </label>
                                        </div>
                                    ) : (
                                        <div className='form-group form-check'>
                                            <input
                                                className='form-check-input'
                                                id='bathroom'
                                                type='checkbox'
                                                name='bathroom'
                                                value={data.has_bathroom}
                                                onChange={handleChange}
                                            />
                                            <label htmlFor='bathroom'>
                                                Nhà tắm
                                            </label>
                                        </div>
                                    )}
                                    {data.has_kitchen ? (
                                        <div className='form-group form-check'>
                                            <input
                                                className='form-check-input'
                                                id='kitchen'
                                                type='checkbox'
                                                name='kitchen'
                                                value={data.has_kitchen}
                                                onChange={handleChange}
                                                defaultChecked
                                            />
                                            <label htmlFor='kitchen'>
                                                Nhà bếp
                                            </label>
                                        </div>
                                    ) : (
                                        <div className='form-group form-check'>
                                            <input
                                                className='form-check-input'
                                                id='kitchen'
                                                type='checkbox'
                                                name='kitchen'
                                                value={data.has_kitchen}
                                                onChange={handleChange}
                                            />
                                            <label htmlFor='kitchen'>
                                                Nhà bếp
                                            </label>
                                        </div>
                                    )}
                                </div>
                                <div className='col-sm-6'>
                                    {data.has_waterHeater ? (
                                        <div className='form-group form-check'>
                                            <input
                                                className='form-check-input'
                                                id='waterHeater'
                                                type='checkbox'
                                                name='waterHeater'
                                                value={data.has_waterHeater}
                                                onChange={handleChange}
                                                defaultChecked
                                            />
                                            <label htmlFor='waterHeater'>
                                                Nóng lạnh
                                            </label>
                                        </div>
                                    ) : (
                                        <div className='form-group form-check'>
                                            <input
                                                className='form-check-input'
                                                id='waterHeater'
                                                type='checkbox'
                                                name='waterHeater'
                                                value={data.has_waterHeater}
                                                onChange={handleChange}
                                            />
                                            <label htmlFor='waterHeater'>
                                                Nóng lạnh
                                            </label>
                                        </div>
                                    )}
                                    {data.has_airConditioner ? (
                                        <div className='form-group form-check'>
                                            <input
                                                className='form-check-input'
                                                id='airConditioner'
                                                type='checkbox'
                                                name='airConditioner'
                                                value={data.has_airConditioner}
                                                onChange={handleChange}
                                                defaultChecked
                                            />
                                            <label htmlFor='airConditioner'>
                                                Điều hòa
                                            </label>
                                        </div>
                                    ) : (
                                        <div className='form-group form-check'>
                                            <input
                                                className='form-check-input'
                                                id='airConditioner'
                                                type='checkbox'
                                                name='airConditioner'
                                                value={data.has_airConditioner}
                                                onChange={handleChange}
                                            />
                                            <label htmlFor='airConditioner'>
                                                Điều hòa
                                            </label>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className='col-sm-4'>
                            <br />
                            <h5>Thời gian đăng bài (số tuần)</h5>
                            <div className='form-group'>
                                <input
                                    className='form-control'
                                    id='time_post'
                                    type='text'
                                    name='time_post'
                                    value={data.time_post}
                                    onChange={handleChange}
                                    placeholder='1-4 tuần'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.time_post}
                                </span>
                            </div>
                        </div>
                        <div className='col-sm-4'>
                            <br />
                            <h5>Tình trạng phòng</h5>
                            {data.rented_status ? (
                                <div className='form-group form-check'>
                                    <input
                                        className='form-check-input'
                                        id='rented_status'
                                        type='checkbox'
                                        name='rented_status'
                                        value={data.rented_status}
                                        onChange={handleChange}
                                        defaultChecked
                                    />
                                    <label htmlFor='airConditioner'>
                                        Đã có người thuê
                                    </label>
                                </div>
                            ) : (
                                <div className='form-group form-check'>
                                    <input
                                        className='form-check-input'
                                        id='rented_status'
                                        type='checkbox'
                                        name='rented_status'
                                        value={data.rented_status}
                                        onChange={handleChange}
                                    />
                                    <label htmlFor='airConditioner'>
                                        Đã có người thuê
                                    </label>
                                </div>
                            )}
                        </div>
                    </div>
                    <br />
                    <button className='btn btn-info btn-footer'>Sửa đổi</button>
                </form>
            </div>
            <Footer />
        </div>
    );
};

export default EditPost;
