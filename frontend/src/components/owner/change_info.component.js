import React, { useEffect, useState } from 'react';
import axios from 'axios';
import OwnerNav from '../../components/common/owner_nav.component';
import dotenv from 'dotenv';
dotenv.config();

const ChangeInfo = () => {
    const [data, setData] = useState({
        email: '',
        name: '',
        id_card_number: '',
        phone: '',
        address: '',
        role: 'owner',
        errors: {},
    });

    useEffect(() => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/owner/info`, {
                withCredentials: true,
            })
            .then((res) => {
                // console.log(res.data);
                setData({ ...res.data, errors: {} });
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const validation = () => {
        let errors = {};
        let formValidation = true;
        const email_re =
            /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

        if (!data.email) {
            formValidation = false;
            errors.email = 'Không được để trống';
        }
        if (data.email && !data.email.match(email_re)) {
            formValidation = false;
            errors.email = 'Email không đúng định dạng';
        }
        if (!data.name) {
            formValidation = false;
            errors.name = 'Không được để trống';
        }
        if (data.name && (data.name.length < 5 || data.name.length > 50)) {
            formValidation = false;
            errors.name = 'Tên tối thiểu 6 kí tự và ít hơn 50 kí tự';
        }
        if (!data.id_card_number) {
            formValidation = false;
            errors.id_card_number = 'Không được để trống';
        }
        if (data.id_card_number && data.id_card_number.length !== 12) {
            formValidation = false;
            errors.id_card_number = 'Căn cước công dân phải có 12 chữ số';
        }
        if (!data.phone) {
            formValidation = false;
            errors.phone = 'Không được để trống';
        }
        if (data.phone && data.phone.length !== 10) {
            formValidation = false;
            errors.phone = 'Số điện thoại phải có 10 chữ số';
        }
        if (!data.address) {
            formValidation = false;
            errors.address = 'Không được để trống';
        }
        if (data.address && data.address.length >= 255) {
            formValidation = false;
            errors.address = 'Địa chỉ ít hơn 255 kí tự';
        }

        setData({
            ...data,
            errors: errors,
        });
        return formValidation;
    };

    const handleChange = (e) => {
        const value = e.target.value;
        setData({
            ...data,
            [e.target.name]: value,
        });
    };

    const onSubmit = (e) => {
        e.preventDefault();

        const dataChange = {
            email: data.email,
            name: data.name,
            id_card_number: data.id_card_number,
            phone: data.phone,
            address: data.address,
        };

        if (validation()) {
            axios
                .put(
                    `${process.env.REACT_APP_HOST_URL}/user/owner/info`,
                    dataChange,
                    { withCredentials: true }
                )
                .then((res) => {
                    // console.log(res.data);
                    if (res.status === 200) {
                        alert(
                            'Chinh sửa thành công! Hãy chờ đợi admin phê duyệt'
                        );
                        localStorage.setItem('user', res.data.name);
                    }
                    window.location = '/user/owner';
                })
                .catch((err) => {
                    if (err.response.status === 401) {
                        alert(
                            'Bạn không có quyền làm điều này. Hãy liên hệ admin để xin cấp quyền chỉnh sửa tài khoản.'
                        );
                    } else {
                        console.log(err);
                    }
                });
        }
    };

    return (
        <div>
            <OwnerNav />
            <br />
            <br />
            <div className='signup-container col-md-6 col-lg-5 col-sm-7 col-xl-4'>
                <div className='signup-block col-sm-11'>
                    <h2 className='signup'>Thay đổi thông tin</h2>
                    <br />
                    <form
                        action='/signup/owner'
                        method='POST'
                        onSubmit={onSubmit}
                    >
                        <div className='form-group'>
                            <label htmlFor='email'>Email</label>
                            <input
                                className='form-control'
                                id='email'
                                // type='email'
                                name='email'
                                value={data.email}
                                onChange={handleChange}
                                placeholder='Email'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.email}
                            </span>
                        </div>
                        <div className='form-group'>
                            <label htmlFor='name'>Họ tên</label>
                            <input
                                className='form-control'
                                id='name'
                                type='text'
                                name='name'
                                value={data.name}
                                onChange={handleChange}
                                placeholder='Tên tài khoản'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.name}
                            </span>
                        </div>
                        <div className='form-group'>
                            <label htmlFor='id_card_number'>Số CMT/CCCD</label>
                            <input
                                className='form-control'
                                id='id_card_number'
                                type='text'
                                name='id_card_number'
                                value={data.id_card_number}
                                onChange={handleChange}
                                placeholder='CMT/ CCCD'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.id_card_number}
                            </span>
                        </div>
                        <div className='form-group'>
                            <label htmlFor='phone'>Số điện thoại</label>
                            <input
                                className='form-control'
                                id='phone'
                                type='number'
                                name='phone'
                                value={data.phone}
                                onChange={handleChange}
                                placeholder='Số điện thoại'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.phone}
                            </span>
                        </div>
                        <div className='form-group'>
                            <label htmlFor='address'>Địa chỉ</label>
                            <input
                                className='form-control'
                                id='address'
                                type='text'
                                name='address'
                                value={data.address}
                                onChange={handleChange}
                                placeholder='Địa chỉ'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.address}
                            </span>
                        </div>
                        <div className='text-center'>
                            <button className='btn btn-info'>Xác nhận</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default ChangeInfo;
