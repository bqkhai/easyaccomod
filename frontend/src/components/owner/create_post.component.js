import React, { useState } from 'react';
import axios from 'axios';
import OwnerNav from '../common/owner_nav.component';

import dotenv from 'dotenv';
dotenv.config();

const CreatePost = () => {
    const [data, setData] = useState({
        title: '',
        city: '',
        district: '',
        ward: '',
        street: '',
        room_type: 'Phòng trọ',
        room_price: '',
        room_area: '',
        image: [],
        has_bathroom: false,
        has_kitchen: false,
        has_airConditioner: false,
        has_waterHeater: false,
        electricity_price: '',
        water_price: '',
        time_post: '',
        errors: {},
    });

    const handleChange = (e) => {
        const value =
            e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        setData({
            ...data,
            [e.target.name]: value,
        });
    };

    const handleChangeImages = (e) => {
        console.log(e.target.files);
        let files = e.target.files;
        let img = [];
        for (const file in files) {
            img.push(files[file]);
        }
        setData({
            ...data,
            image: img,
        });
        // console.log(data.image);
        console.log(data);
    };

    const validation = () => {
        let errors = {};
        let formValidation = true;

        const string_re =
            /^[0-9a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
        const area_re = /^[1-9][0-9]*[,.]{0,1}[0-9]*$/;
        const money_re = /^[1-9][0-9]*$/;
        const time_post_re = /^[1-4]/;

        if (!data.title) {
            formValidation = false;
            errors.title = 'Không được bỏ trống';
        }
        if (data.title && !data.title.match(string_re)) {
            formValidation = false;
            errors.title = 'Trường này không đúng định dạng';
        }

        if (!data.city) {
            formValidation = false;
            errors.city = 'Không được bỏ trống';
        }
        if (data.city && !data.city.match(string_re)) {
            formValidation = false;
            errors.city = 'Trường này không đúng định dạng';
        }

        if (!data.district) {
            formValidation = false;
            errors.district = 'Không được bỏ trống';
        }
        if (data.district && !data.district.match(string_re)) {
            formValidation = false;
            errors.district = 'Trường này không đúng định dạng';
        }

        if (!data.ward) {
            formValidation = false;
            errors.ward = 'Không được bỏ trống';
        }
        if (data.ward && !data.ward.match(string_re)) {
            formValidation = false;
            errors.ward = 'Trường này không đúng định dạng';
        }

        if (!data.street) {
            formValidation = false;
            errors.street = 'Không được bỏ trống';
        }
        if (data.street && !data.street.match(string_re)) {
            formValidation = false;
            errors.street = 'Trường này không đúng định dạng';
        }

        if (!data.room_type) {
            formValidation = false;
            errors.room_type = 'Không được bỏ trống';
        }
        if (
            data.room_type &&
            data.room_type !== 'Nhà nguyên căn' &&
            data.room_type !== 'Chung cư mini' &&
            data.room_type !== 'Chung cư' &&
            data.room_type !== 'Phòng trọ' &&
            data.room_type !== 'Nhà trọ giá rẻ'
        ) {
            formValidation = false;
            errors.room_type = 'Dữ liệu sai định dạng';
        }

        if (!data.room_price) {
            formValidation = false;
            errors.room_price = 'Không được bỏ trống';
        }
        if (data.room_price && !data.room_price.match(money_re)) {
            formValidation = false;
            errors.room_price = 'Giá thuê cần là số';
        }

        if (!data.room_area) {
            formValidation = false;
            errors.room_area = 'Không được bỏ trống';
        }
        if (data.room_area && !data.room_area.match(area_re)) {
            formValidation = false;
            errors.room_area = 'Diện tích là số';
        }

        if (!data.electricity_price) {
            formValidation = false;
            errors.electricity_price = 'Không được bỏ trống';
        }
        if (data.electricity_price && !data.electricity_price.match(money_re)) {
            formValidation = false;
            errors.electricity_price = 'Giá điện cần là số';
        }

        if (!data.water_price) {
            formValidation = false;
            errors.water_price = 'Không được bỏ trống';
        }
        if (data.water_price && !data.water_price.match(money_re)) {
            formValidation = false;
            errors.water_price = 'Giá nước cần là số';
        }

        if (!data.time_post) {
            formValidation = false;
            errors.time_post = 'Không được bỏ trống';
        }
        if (data.time_post && !data.time_post.match(time_post_re)) {
            formValidation = false;
            errors.time_post = 'Số tuần đăng từ 1-4';
        }

        setData({
            ...data,
            errors: errors,
        });
        return formValidation;
    };

    const onSubmit = (e) => {
        e.preventDefault();

        let postData = new FormData();

        postData.set('title', data.title);
        postData.set('city', data.city);
        postData.set('district', data.district);
        postData.set('ward', data.ward);
        postData.set('street', data.street);
        postData.set('room_type', data.room_type);
        postData.set('room_price', data.room_price);
        postData.set('room_area', data.room_area);
        postData.set('has_bathroom', data.has_bathroom);
        postData.set('has_kitchen', data.has_kitchen);
        postData.set('has_airConditioner', data.has_airConditioner);
        postData.set('has_waterHeater', data.has_waterHeater);
        postData.set('electricity_price', data.electricity_price);
        postData.set('water_price', data.water_price);
        postData.set('time_post', data.time_post);
        data.image.forEach((img) => {
            postData.append('image', img);
        });

        console.log(postData);

        if (validation()) {
            axios
                .post(
                    `${process.env.REACT_APP_HOST_URL}/user/owner/post`,
                    postData,
                    {
                        withCredentials: true,
                    }
                )
                .then((res) => {
                    if (res.status === 201) {
                        alert('Thành công! Hãy chờ đợi admin phê duyệt');
                        window.location.href = `${process.env.REACT_APP_FE_URL}/user/owner/posts`;
                    }
                })
                .catch((err) => {
                    if (err.response.status === 401) {
                        console.log(err.response.data);
                        alert('Bạn không có quyền làm điều này');
                    } else {
                        console.log(err);
                    }
                });
        }
    };

    return (
        <div className='content'>
            <OwnerNav />
            <div className='container'>
                <form
                    action=''
                    method='POST'
                    onSubmit={onSubmit}
                    encType='multipart/form-data'
                >
                    <div>
                        <br />
                        <h4>Tiêu đề</h4>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='title'
                                type='text'
                                name='title'
                                value={data.title}
                                onChange={handleChange}
                                placeholder='Tiêu đề'
                            ></input>
                            <span style={{ color: 'red' }}>
                                {data.errors.title}
                            </span>
                        </div>
                        <br />
                    </div>
                    <div className='row'>
                        <div className='col-sm-6'>
                            <h4>Địa chỉ</h4>
                            <div className='form-group'>
                                <input
                                    className='form-control'
                                    id='city'
                                    type='text'
                                    name='city'
                                    value={data.city}
                                    onChange={handleChange}
                                    placeholder='Thành phố'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.city}
                                </span>
                            </div>
                            <div className='form-group'>
                                <input
                                    className='form-control'
                                    id='district'
                                    type='text'
                                    name='district'
                                    value={data.district}
                                    onChange={handleChange}
                                    placeholder='Quận'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.district}
                                </span>
                            </div>
                            <div className='form-group'>
                                <input
                                    className='form-control'
                                    id='ward'
                                    type='text'
                                    name='ward'
                                    value={data.ward}
                                    onChange={handleChange}
                                    placeholder='Phường'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.ward}
                                </span>
                            </div>
                            <div className='form-group'>
                                <input
                                    className='form-control'
                                    id='street'
                                    type='text'
                                    name='street'
                                    value={data.street}
                                    onChange={handleChange}
                                    placeholder='Đường'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.street}
                                </span>
                            </div>
                        </div>
                        <div className='col-sm-6'>
                            <h4>Mô tả phòng</h4>
                            <div className='form-group'>
                                <select
                                    className='form-control'
                                    id='room_type'
                                    type='text'
                                    name='room_type'
                                    value={data.room_type}
                                    onChange={handleChange}
                                >
                                    <option>Nhà nguyên căn</option>
                                    <option>Chung cư mini</option>
                                    <option>Chung cư</option>
                                    <option>Phòng trọ</option>
                                    <option>Nhà trọ giá rẻ</option>
                                </select>
                                <span style={{ color: 'red' }}>
                                    {data.errors.room_type}
                                </span>
                            </div>
                            <div className='form-group'>
                                <input
                                    className='form-control'
                                    id='room_area'
                                    type='text'
                                    name='room_area'
                                    value={data.room_area}
                                    onChange={handleChange}
                                    placeholder='Diện tích (mét vuông)'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.room_area}
                                </span>
                            </div>
                            <div className='form-group'>
                                <input
                                    className='form-control'
                                    id='room_price'
                                    type='text'
                                    name='room_price'
                                    value={data.room_price}
                                    onChange={handleChange}
                                    placeholder='Giá nguyên căn hoặc giá thuê tháng '
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.room_price}
                                </span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div className='row'>
                        <div className='col-sm-2'>
                            <h4>Tiện ích</h4>
                            <div className='form-group form-check'>
                                <input
                                    className='form-check-input'
                                    id='has_bathroom'
                                    type='checkbox'
                                    name='has_bathroom'
                                    value={data.has_bathroom}
                                    onChange={handleChange}
                                    // checked={isChecked.has_bathroom}
                                    // onChange={handleChangeCheckbox}
                                />
                                <label htmlFor='bathroom'>Nhà tắm</label>
                            </div>
                            <div className='form-group form-check'>
                                <input
                                    className='form-check-input'
                                    id='has_kitchen'
                                    type='checkbox'
                                    name='has_kitchen'
                                    value={data.has_kitchen}
                                    onChange={handleChange}
                                    // checked={isChecked.has_kitchen}
                                    // onChange={handleChangeCheckbox}
                                />
                                <label htmlFor='kitchen'>Nhà bếp</label>
                            </div>
                            <div className='form-group form-check'>
                                <input
                                    className='form-check-input'
                                    id='has_waterHeater'
                                    type='checkbox'
                                    name='has_waterHeater'
                                    value={data.has_waterHeater}
                                    onChange={handleChange}
                                    // checked={isChecked.has_waterHeater}
                                    // onChange={handleChangeCheckbox}
                                />
                                <label htmlFor='waterHeater'>Nóng lạnh</label>
                            </div>
                            <div className='form-group form-check'>
                                <input
                                    className='form-check-input'
                                    id='has_airConditioner'
                                    type='checkbox'
                                    name='has_airConditioner'
                                    value={data.has_airConditioner}
                                    onChange={handleChange}
                                    // checked={isChecked.has_airConditioner}
                                    // onChange={handleChangeCheckbox}
                                />
                                <label htmlFor='airConditioner'>Điều hòa</label>
                            </div>
                        </div>
                        <div className='col-sm-5'>
                            <h4>Giá cả (vnđ)</h4>
                            <div className='form-group'>
                                <input
                                    className='form-control'
                                    id='electricity_price'
                                    type='number'
                                    name='electricity_price'
                                    value={data.electricity_price}
                                    onChange={handleChange}
                                    placeholder='Giá điện VNĐ/kWh'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.electricity_price}
                                </span>
                            </div>
                            <div className='form-group'>
                                <input
                                    className='form-control'
                                    id='water_price'
                                    type='number'
                                    name='water_price'
                                    value={data.water_price}
                                    onChange={handleChange}
                                    placeholder='Giá nước VNĐ/khối'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.water_price}
                                </span>
                            </div>
                        </div>
                        <div className='col-sm-5'>
                            <h4>Thời gian đăng bài (số tuần)</h4>
                            <div className='form-group'>
                                <input
                                    className='form-control'
                                    id='time_post'
                                    type='number'
                                    name='time_post'
                                    value={data.time_post}
                                    onChange={handleChange}
                                    placeholder='Thời gian đăng (số tuần)'
                                ></input>
                                <span style={{ color: 'red' }}>
                                    {data.errors.time_post}
                                </span>
                            </div>
                            <div className='form-group'>
                                <h4>Ảnh mô tả</h4>
                                <input
                                    id='image'
                                    type='file'
                                    name='image'
                                    alt='preview'
                                    multiple
                                    accept='image/*'
                                    onChange={handleChangeImages}
                                    // onChange={handleChange}
                                ></input>
                            </div>
                        </div>
                    </div>
                    <br />
                    <button className='btn btn-info btn-footer'>
                        Thêm mới
                    </button>
                </form>
            </div>
        </div>
    );
};

export default CreatePost;
