import React from 'react';
import { Link } from 'react-router-dom';

import './home.css';

const Home = () => {
    return (
        <div className='home-layout'>
            <nav className='navbar navbar-dark navbar-expand-lg'>
                <div className='container'>
                    <Link to='/' className='navbar-brand'>
                        HouseForRent
                    </Link>
                    <ul className='navbar-nav navbar-right'>
                        <li className='navbar-item'>
                            <Link to='/signup' className='nav-link text-light'>
                                <button className='d-block btn btn-outline-light'>
                                    Đăng ký
                                </button>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
            {/* <BaseNav /> */}
            <div className='title'>
                <div className='container'>
                    <h1 className='text-light home-slogan col-sm-11'>
                        Tìm kiếm phòng trọ
                    </h1>
                    <br />
                    <p className='text-light home-content col-xl-9 col-lg-10 col-md-11 col-sm-11'></p>
                    <div className='col-sm-5 col-lg-3'>
                        <Link
                            to='/login'
                            className='nav-link text-light login-button'
                        >
                            <button className='d-block btn btn-light col-sm-12 text-info'>
                                Đăng nhập
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;
