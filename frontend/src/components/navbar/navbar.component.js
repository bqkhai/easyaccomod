import React from 'react';
import { Link } from 'react-router-dom';
import './navbar.css';

const BaseNav = () => {
    return (
        <div>
            <nav className='navbar navbar-dark navbar-expand-lg user-nav'>
                <div className='container'>
                    <Link to='/' className='navbar-brand'>
                        HouseForRent
                    </Link>
                    <ul className='navbar-nav navbar-right'>
                        <li className='navbar-item'>
                            <Link to='/login' className='nav-link text-light'>
                                <button className='d-block btn btn-outline-light'>
                                    Đăng nhập
                                </button>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    );
};

export default BaseNav;
