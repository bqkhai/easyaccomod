import React, { useState } from 'react';
import axios from 'axios';
import OwnerNav from '../../components/common/owner_nav.component';
import AdminNav from '../../components/common/admin_nav.component';
import RenterNav from '../../components/common/renter_nav.component';
import dotenv from 'dotenv';
dotenv.config();

const ChangePassword = () => {
    const [data, setData] = useState({
        current_password: '',
        new_password: '',
        confirm_password: '',
        errors: {},
    });

    const handleChange = (e) => {
        const value = e.target.value;
        setData({
            ...data,
            [e.target.name]: value,
        });
    };

    const validation = () => {
        let errors = {};
        let formValidation = true;
        const password_re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{6,20}$/;

        if (!data.current_password) {
            formValidation = false;
            errors.current_password = 'Không được để trống';
        }

        if (
            data.current_password &&
            !data.current_password.match(password_re)
        ) {
            formValidation = false;
            errors.password = 'Mật khẩu phải có từ 6-20 kí tự';
        }

        if (!data.new_password) {
            formValidation = false;
            errors.new_password = 'Không được để trống';
        }

        if (data.new_password && !data.new_password.match(password_re)) {
            formValidation = false;
            errors.new_password = 'Mật khẩu phải có từ 6-20 kí tự';
        }

        if (!data.confirm_password) {
            formValidation = false;
            errors.confirm_password = 'Không được để trống';
        }

        if (
            data.confirm_password &&
            !data.confirm_password.match(password_re)
        ) {
            formValidation = false;
            errors.confirm_password = 'Mật khẩu phải có từ 6-20 kí tự';
        }

        if (data.new_password !== data.confirm_password) {
            formValidation = false;
            errors.matchPassword = '.Mật khẩu xác nhận không khớp';
        }

        setData({
            ...data,
            errors: errors,
        });
        return formValidation;
    };

    const onSubmit = (e) => {
        e.preventDefault();

        const dataPass = {
            current_password: data.current_password,
            new_password: data.new_password,
            confirm_password: data.confirm_password,
        };

        if (validation()) {
            axios
                .patch(
                    `${process.env.REACT_APP_HOST_URL}/user/change-password`,
                    dataPass,
                    {
                        withCredentials: true,
                    }
                )
                .then((res) => {
                    console.log(res);
                    alert('Thay đổi mật khẩu thành công!');
                    window.location.reload();
                })
                .catch((err) => console.log(err));
        }
    };

    return (
        <div>
            {localStorage.getItem('user_role') === 'admin' ? (
                <AdminNav />
            ) : localStorage.getItem('user_role') === 'owner' ? (
                <OwnerNav />
            ) : localStorage.getItem('user_role') === 'renter' ? (
                <RenterNav />
            ) : null}
            <br />
            <br />
            <div className='form-container col-md-6 col-lg-5 col-sm-7 col-xl-4'>
                <div className='form-block col-sm-11'>
                    <h2 className='header'>Thay đổi mật khẩu</h2>
                    <br />
                    <form method='PATCH' onSubmit={onSubmit} id='form'>
                        <div className='form-group'>
                            <label htmlFor='current_password'>
                                Mật khẩu hiện tại
                            </label>
                            <input
                                className='form-control'
                                id='current_password'
                                type='password'
                                name='current_password'
                                value={data.current_password}
                                onChange={handleChange}
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.current_password}
                            </span>
                        </div>
                        <div className='form-group'>
                            <label htmlFor='new_password'>Mật khẩu mới</label>
                            <input
                                className='form-control'
                                id='new_password'
                                type='password'
                                name='new_password'
                                value={data.new_password}
                                onChange={handleChange}
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.new_password}
                            </span>
                        </div>
                        <div className='form-group'>
                            <label htmlFor='cf_pass'>Xác nhận lại</label>
                            <input
                                className='form-control'
                                id='confirm_password'
                                type='password'
                                name='confirm_password'
                                value={data.confirm_password}
                                onChange={handleChange}
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.confirm_password}{' '}
                                {data.errors.matchPassword}
                            </span>
                        </div>
                        <div className='text-center'>
                            <button className='btn btn-info'>Gửi</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default ChangePassword;
