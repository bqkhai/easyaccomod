import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import BaseNav from '../../navbar/navbar.component';

import '../../../css/signup.css';

import dotenv from 'dotenv';
dotenv.config();

const SignupOwner = () => {
    const [data, setData] = useState({
        email: '',
        name: '',
        id_card_number: '',
        phone: '',
        address: '',
        password: '',
        cf_pass: '',
        role: 'owner',
        errors: {},
    });

    const handleChange = (e) => {
        const value = e.target.value;
        setData({
            ...data,
            [e.target.name]: value,
        });
    };

    const validation = () => {
        let errors = {};
        let formValidation = true;
        const email_re =
            /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        const password_re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{6,20}$/;

        if (!data.email) {
            formValidation = false;
            errors.email = 'Không được để trống';
        }
        if (data.email && !data.email.match(email_re)) {
            formValidation = false;
            errors.email = 'Email không đúng định dạng';
        }
        if (!data.name) {
            formValidation = false;
            errors.name = 'Không được để trống';
        }
        if (data.name && (data.name.length < 5 || data.name.length > 50)) {
            formValidation = false;
            errors.name = 'Tên tối thiểu 6 kí tự và ít hơn 50 kí tự';
        }
        if (!data.id_card_number) {
            formValidation = false;
            errors.id_card_number = 'Không được để trống';
        }
        if (data.id_card_number && data.id_card_number.length !== 12) {
            formValidation = false;
            errors.id_card_number = 'Căn cước công dân phải có 12 chữ số';
        }
        if (!data.phone) {
            formValidation = false;
            errors.phone = 'Không được để trống';
        }
        if (data.phone && data.phone.length !== 10) {
            formValidation = false;
            errors.phone = 'Số điện thoại phải có 10 chữ số';
        }
        if (!data.address) {
            formValidation = false;
            errors.address = 'Không được để trống';
        }
        if (data.address && data.address.length >= 255) {
            formValidation = false;
            errors.address = 'Địa chỉ ít hơn 255 kí tự';
        }
        if (!data.password) {
            formValidation = false;
            errors.password = 'Không được để trống';
        }
        if (data.password && !data.password.match(password_re)) {
            formValidation = false;
            errors.password = 'Mật khẩu phải có 6-20 kí tự';
        }
        if (!data.cf_pass) {
            formValidation = false;
            errors.cf_pass = 'Không được để trống';
        }
        if (data.cf_pass && data.password !== data.cf_pass) {
            formValidation = false;
            errors.cf_pass = 'Mật khẩu không khớp';
        }

        setData({
            ...data,
            errors: errors,
        });
        return formValidation;
    };

    const onSubmit = (e) => {
        e.preventDefault();

        const dataOwner = {
            email: data.email,
            name: data.name,
            id_card_number: data.id_card_number,
            phone: data.phone,
            address: data.address,
            password: data.password,
            cf_pass: data.cf_pass,
            role: 'owner',
        };

        if (validation()) {
            axios
                .post(`${process.env.REACT_APP_HOST_URL}/signup/`, dataOwner)
                .then((res) => {
                    if (res.status === 200) {
                        console.log(res.data);
                        alert(res.data.errors);
                    }
                    if (res.status === 201) {
                        alert('Đăng ký thành công! Hãy đăng nhập');
                        window.location = '/login';
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    };

    return (
        <div>
            <BaseNav />
            <br />
            <br />
            <div className='signup-container col-md-6 col-lg-5 col-sm-7 col-xl-4'>
                <div className='signup-block col-sm-11'>
                    <h2 className='signup'>Đăng ký tài khoản</h2>
                    <br />
                    <form
                        action='/signup/owner'
                        method='POST'
                        onSubmit={onSubmit}
                    >
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='email'
                                // type='email'
                                name='email'
                                value={data.email}
                                onChange={handleChange}
                                placeholder='Email'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.email}
                            </span>
                        </div>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='name'
                                // type='text'
                                name='name'
                                value={data.name}
                                onChange={handleChange}
                                placeholder='Tên tài khoản'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.name}
                            </span>
                        </div>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='id_card_number'
                                // type='text'
                                name='id_card_number'
                                value={data.id_card_number}
                                onChange={handleChange}
                                placeholder='CMT/ CCCD'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.id_card_number}
                            </span>
                        </div>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='phone'
                                type='number'
                                name='phone'
                                value={data.phone}
                                onChange={handleChange}
                                placeholder='Số điện thoại'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.phone}
                            </span>
                        </div>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='address'
                                // type='text'
                                name='address'
                                value={data.address}
                                onChange={handleChange}
                                placeholder='Địa chỉ'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.address}
                            </span>
                        </div>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='password'
                                type='password'
                                name='password'
                                value={data.password}
                                onChange={handleChange}
                                placeholder='Mật khẩu'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.password}
                            </span>
                        </div>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='cf_pass'
                                type='password'
                                name='cf_pass'
                                value={data.cf_pass}
                                onChange={handleChange}
                                placeholder='Nhập lại mật khẩu'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.cf_pass}
                            </span>
                        </div>
                        <div className='text-center'>
                            <button className='btn btn-info dis'>
                                Đăng ký
                            </button>
                            <Link to='/signup/renter'>
                                <button className='btn btn-info'>
                                    Người thuê
                                </button>
                            </Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default SignupOwner;
