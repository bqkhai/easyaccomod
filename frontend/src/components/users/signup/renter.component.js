import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import BaseNav from '../../navbar/navbar.component';

import '../../../css/signup.css';

import dotenv from 'dotenv';
dotenv.config();

const SignupRenter = () => {
    const [data, setData] = useState({
        email: '',
        name: '',
        password: '',
        cf_pass: '',
        role: 'renter',
        errors: {},
    });

    const validation = () => {
        let errors = {};
        let formValidation = true;
        const email_re =
            /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        const password_re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{6,20}$/;

        if (!data.email) {
            formValidation = false;
            errors.email = 'Không được để trống';
        }
        if (data.email && !data.email.match(email_re)) {
            formValidation = false;
            errors.email = 'Email không đúng định dạng';
        }
        if (!data.name) {
            formValidation = false;
            errors.name = 'Không được để trống';
        }
        if (data.name && (data.name.length < 5 || data.name.length > 50)) {
            formValidation = false;
            errors.name = 'Tên tối thiểu 5 kí tự và ít hơn 50 kí tự';
        }
        if (!data.password) {
            formValidation = false;
            errors.password = 'Không được để trống';
        }
        if (data.password && !data.password.match(password_re)) {
            formValidation = false;
            errors.password = 'Mật khẩu phải có 6-20 kí tự';
        }
        if (!data.cf_pass) {
            formValidation = false;
            errors.cf_pass = 'Không được để trống';
        }
        if (data.cf_pass && data.password !== data.cf_pass) {
            formValidation = false;
            errors.cf_pass = 'Mật khẩu không khớp';
        }

        setData({
            ...data,
            errors: errors,
        });
        return formValidation;
    };

    const handleChange = (e) => {
        const value = e.target.value;
        setData({
            ...data,
            [e.target.name]: value,
        });
    };

    const onSubmit = (e) => {
        e.preventDefault();

        const dataRenter = {
            email: data.email,
            name: data.name,
            password: data.password,
            cf_pass: data.cf_pass,
            role: 'renter',
        };

        if (validation()) {
            axios
                .post(`${process.env.REACT_APP_HOST_URL}/signup/`, dataRenter)
                .then((res) => {
                    if (res.status === 200) {
                        console.log(res.data.errors);
                        alert(res.data.errors);
                    }
                    if (res.status === 201) {
                        alert('Đăng ký thành công! Hãy đăng nhập');
                        window.location = '/login';
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    };
    return (
        <div>
            <BaseNav />
            <br />
            <br />
            <div className='signup-container col-md-6 col-lg-5 col-sm-7 col-xl-4'>
                <div className='signup-block col-sm-11'>
                    <h2 className='signup'>Đăng ký tài khoản</h2>
                    <br />
                    <form
                        action='/signup/renter'
                        method='POST'
                        onSubmit={onSubmit}
                    >
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='email'
                                // type='email'
                                name='email'
                                value={data.email}
                                onChange={handleChange}
                                placeholder='Email'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.email}
                            </span>
                        </div>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='name'
                                type='text'
                                name='name'
                                value={data.name}
                                onChange={handleChange}
                                placeholder='Tên tài khoản'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.name}
                            </span>
                        </div>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='password'
                                type='password'
                                name='password'
                                value={data.password}
                                onChange={handleChange}
                                placeholder='Mật khẩu'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.password}
                            </span>
                        </div>
                        <div className='form-group'>
                            <input
                                className='form-control'
                                id='cf_pass'
                                type='password'
                                name='cf_pass'
                                value={data.cf_pass}
                                onChange={handleChange}
                                placeholder='Nhập lại mật khẩu'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.cf_pass}
                            </span>
                        </div>
                        <div className='text-center'>
                            <button className='btn btn-info dis'>
                                Đăng ký
                            </button>
                            <Link to='/signup/owner'>
                                <button className='btn btn-info'>
                                    Chủ trọ
                                </button>
                            </Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default SignupRenter;
