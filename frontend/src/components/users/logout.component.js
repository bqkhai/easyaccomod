import React from 'react';
import axios from 'axios';
import dotenv from 'dotenv';
dotenv.config();

const Logout = () => {
    const onClickLogout = () => {
        axios
            .post(
                `${process.env.REACT_APP_HOST_URL}/logout`,
                {},
                { withCredentials: true }
            )
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            });

        localStorage.removeItem('favourite');
        localStorage.removeItem('user');
        localStorage.removeItem('user_role');
        window.location = '/login';
    };

    return (
        // eslint-disable-next-line jsx-a11y/anchor-is-valid
        <a className='logout text-dark nav-link' onClick={onClickLogout}>
            Đăng xuất
        </a>
    );
};

export default Logout;
