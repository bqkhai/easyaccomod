import React from 'react';
import { Link } from 'react-router-dom';

const Signup = () => {
    return (
        <div className=''>
            <nav className='navbar navbar-dark navbar-expand-lg user-nav'>
                <div className='container'>
                    <Link to='/' className='navbar-brand'>
                        HouseForRent
                    </Link>
                    <ul className='navbar-nav navbar-right'>
                        <li className='navbar-item'>
                            <Link to='/login' className='nav-link text-light'>
                                <button className='d-block btn btn-outline-light'>
                                    Đăng nhập
                                </button>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
            <br />
            <br />
            <br />
            <div className='form-container col-md-6 col-lg-5 col-sm-7 col-xl-4'>
                <br />
                <div className='form-block col-sm-11'>
                    <div className=''>
                        <div className='text-center'>
                            <img
                                src='../../../home.ico'
                                alt='logo'
                                className='logo'
                            />
                            <h2 className='text-info'>HouseForRent</h2>
                        </div>
                    </div>
                    <br />
                    <p className='slogan'>
                        Hãy đăng ký tài khoản để truy cập HouseForRent{' '}
                    </p>
                    <h3 className='text-center'>Đăng ký cho</h3>
                    <br />
                    <div className='link-signup'>
                        <div className='text-center row'>
                            <div className='text-center col-sm-6'>
                                <Link
                                    to='/signup/renter'
                                    className='d-block btn btn-info'
                                >
                                    Người thuê trọ
                                </Link>
                            </div>
                            <div className='text-center col-sm-6'>
                                <Link
                                    to='/signup/owner'
                                    className='d-block btn btn-info'
                                >
                                    Chủ trọ
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Signup;
