import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import dotenv from 'dotenv';
dotenv.config();

const Login = () => {
    const [data, setData] = useState({
        email: '',
        password: '',
        role: 'owner',
        errors: {},
    });

    const validation = () => {
        let errors = {};
        let formValidation = true;
        const email_re =
            /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        const password_re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{6,13}$/;

        if (!data.email) {
            formValidation = false;
            errors.email = 'Không được để trống';
        }
        if (data.email && !data.email.match(email_re)) {
            formValidation = false;
            errors.email = 'Email không đúng định dạng';
        }
        if (!data.password) {
            formValidation = false;
            errors.password = 'Không được để trống';
        }
        if (data.password && !data.password.match(password_re)) {
            formValidation = false;
            errors.password = 'Mật khẩu không đúng định dạng';
        }

        setData({
            ...data,
            errors: errors,
        });
        return formValidation;
    };

    const handleChange = (e) => {
        const value = e.target.value;
        setData({
            ...data,
            [e.target.name]: value,
        });
    };

    const onSubmit = (e) => {
        e.preventDefault();

        const dataUser = {
            email: data.email,
            password: data.password,
            role: data.role,
        };

        if (validation()) {
            axios
                .post(`${process.env.REACT_APP_HOST_URL}/login`, dataUser, {
                    withCredentials: true,
                })
                .then((res) => {
                    console.log(res.data);
                    // debugger;
                    if (res.status === 200 && res.data.name && res.data.role) {
                        localStorage.setItem('user', res.data.name);
                        localStorage.setItem('user_role', res.data.role);
                    }
                    if (res.status === 200 && res.data.favourite) {
                        console.log(res.data.favourite);
                        // debugger;
                        localStorage.setItem('favourite', res.data.favourite);
                    }
                    if (res.data.email) {
                        window.location = `/user/${res.data.role}`;
                    }
                })
                .catch((err) => console.log(err));
        }
    };

    return (
        <div className=''>
            <nav className='navbar navbar-dark navbar-expand-lg user-nav'>
                <div className='container'>
                    <Link to='/' className='navbar-brand'>
                        HouseForRent
                    </Link>
                    <ul className='navbar-nav navbar-right'>
                        <li className='navbar-item'>
                            <Link to='/signup' className='nav-link text-light'>
                                <button className='d-block btn btn-outline-light'>
                                    Đăng ký
                                </button>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
            <br />
            <br />
            <div className='login-container col-md-6 col-lg-5 col-sm-7 col-xl-4'>
                <div className='login-block col-sm-11'>
                    <br />
                    <h2 className='login'>Đăng nhập</h2>
                    <form method='POST' onSubmit={onSubmit}>
                        <div className='form-group'>
                            <label htmlFor='email'>Email</label>
                            <input
                                className='form-control'
                                id='email'
                                // type='email'
                                name='email'
                                value={data.email}
                                onChange={handleChange}
                                placeholder='abc@gmail.com'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.email}
                            </span>
                        </div>
                        <div className='form-group'>
                            <label htmlFor='password'>Mật khẩu</label>
                            <input
                                className='form-control '
                                id='password'
                                type='password'
                                name='password'
                                value={data.password}
                                onChange={handleChange}
                                placeholder='Ít nhất 6 kí tự'
                            />
                            <span style={{ color: 'red' }}>
                                {data.errors.password}
                            </span>
                        </div>
                        <div className='form-group'>
                            {' '}
                            <label htmlFor='role'>Vai trò</label>
                            <select
                                className='form-control'
                                id='role'
                                name='role'
                                value={data.role}
                                onChange={handleChange}
                            >
                                <option value='owner'>Chủ trọ</option>
                                <option value='renter'>Người thuê</option>
                                <option value='admin'>Quản lý</option>
                            </select>
                        </div>
                        <div className='text-center'>
                            <button className='btn btn-info'>Đăng nhập</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Login;
