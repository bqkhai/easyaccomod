import React, { useEffect, useState } from 'react';
import axios from 'axios';
import RenterNav from '../common/renter_nav.component';
import Report from './report.component';
import Review from './review.component';
import dotenv from 'dotenv';
import { useParams } from 'react-router-dom';
import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/styles.css';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
const AutoplaySlider = withAutoplay(AwesomeSlider);

dotenv.config();

const PostDetailRenter = () => {
    const [data, setData] = useState({
        id: '',
        title: '',
        city: '',
        district: '',
        ward: '',
        street: '',
        room_type: '',
        room_price: '',
        room_area: '',
        has_bathroom: '',
        has_kitchen: '',
        has_airConditioner: '',
        has_waterHeater: '',
        electricity_price: '',
        water_price: '',
        image: [],
        time_post: '',
        rented_status: '',
        views: '',
        likes: '',
        reviews: [],
        inFavourites: false,
        owner_name: '',
        owner_phone: '',
        is_approved: '',
    });

    const { id } = useParams();

    useEffect(() => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/renter/posts/${id}`, {
                withCredentials: true,
            })
            .then((res) => {
                console.log(res.data);

                axios
                    .get(
                        `${process.env.REACT_APP_HOST_URL}/user/renter/reviews/${id}`,
                        {
                            withCredentials: true,
                        }
                    )
                    .then((review) => {
                        console.log(review.data);
                        setData({
                            ...res.data,
                            owner_name: res.data.user_id.name,
                            owner_phone: res.data.user_id.phone,
                            reviews: review.data,
                        });
                        if (res.status === 200) {
                            const favourite = localStorage
                                .getItem('favourite')
                                .split(',');
                            favourite.forEach((post_id) => {
                                if (post_id === res.data._id) {
                                    setData({
                                        ...res.data,
                                        owner_name: res.data.user_id.name,
                                        owner_phone: res.data.user_id.phone,
                                        reviews: review.data,
                                        inFavourites: true,
                                    });
                                }
                            });
                        }
                    })
                    .catch((err) => console.log(err));
            })
            .catch((err) => {
                console.log(err);
            });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const addToFavourites = () => {
        axios
            .patch(
                `${process.env.REACT_APP_HOST_URL}/user/renter/favourite/${id}`,
                {},
                { withCredentials: true }
            )
            .then((res) => {
                localStorage.setItem(
                    'favourite',
                    localStorage.getItem('favourite') + ',' + id
                );
                if (res.status === 200) {
                    setData({
                        ...data,
                        inFavourites: true,
                    });
                }
            })
            .catch((err) => console.log(err));
        // loadData();
    };

    const Star = (star) => {
        let arr = [];
        for (let i = 0; i < star; i++) {
            if (i < star) {
                arr[i] = true;
            } else {
                arr[i] = false;
            }
        }
        return arr.map((elem, index) => {
            if (elem === true) {
                return (
                    <span key={index} className='star-rating'>
                        X{' '}
                    </span>
                );
            } // replace with bright star image
            else {
                return <span key={index}>O </span>;
            }
        });
    };

    const showAllReviews = () => {
        return data.reviews.map((review, index) => {
            return (
                <div className='review form-row'>
                    <div className='avatar'>
                        <img
                            className='user_avatar'
                            src={
                                'https://picsum.photos/seed/' +
                                Math.random() +
                                '/200'
                            }
                            width='100%'
                            alt='user-avatar'
                        ></img>
                    </div>
                    <div>
                        <h6 className='renter_name'>{review.user_id.name}</h6>
                        {Star(review.star)}
                        <p className='renter_review'>{review.review}</p>
                    </div>
                </div>
            );
        });
    };

    return (
        <div className='content'>
            <RenterNav />
            <div className='container'>
                <br />
                {/* <h3>Chi tiết phòng</h3> */}
                <br />
                <div className='row'>
                    <div className='col-sm-8 image'>
                        <AutoplaySlider
                            play={true}
                            cancelOnInteraction={false}
                            interval={1000}
                        >
                            {data.image.map((img, index) => (
                                <div key={index} className='yours-custom-class'>
                                    <img
                                        src={img}
                                        width='100%'
                                        alt='room_image'
                                    ></img>
                                </div>
                            ))}
                        </AutoplaySlider>
                    </div>
                    <div className='col-sm-4 text-box'>
                        <h2 className='text-capitalize'>{data.title}</h2>
                        <p className='rented-rate'>
                            {data.room_price} VNĐ/Tháng
                        </p>
                        <br />
                        <div>
                            {/* <h5>Trạng thái</h5> */}
                            {data.rented_status ? (
                                <h5 className='text-danger'>
                                    Đã có người thuê
                                </h5>
                            ) : (
                                <h5 className='text-success'>
                                    Chưa có người thuê
                                </h5>
                            )}
                        </div>
                        {!data.inFavourites ? (
                            <button
                                className='btn btn-info'
                                onClick={addToFavourites}
                            >
                                Thích
                            </button>
                        ) : (
                            <p className='text-primary'>Đã thích</p>
                        )}
                        <br />
                    </div>
                </div>
                <br />
                <br />
                <div className='row'>
                    <div className='col-sm-6'>
                        <h2>Thông tin chung</h2>
                        <br />
                        <div className='form-row'>
                            <i className='fas fa-map-marked-alt'></i>
                            <p>
                                Địa chỉ: {data.street}, {data.ward},{' '}
                                {data.district}, {data.city}
                            </p>
                        </div>
                        <div className='form-row'>
                            <i className='fas fa-home'></i>
                            <p>Loại phòng: {data.room_type}</p>
                        </div>
                        <div className='form-row'>
                            <i className='fas fa-warehouse'></i>
                            <p>Diện tích: {data.area} M2</p>
                        </div>
                        <div className='form-row'>
                            <i className='fas fa-couch'></i>
                            <p>
                                Tiện ích:{' '}
                                {data.has_bathroom ? (
                                    <span>Phòng tắm</span>
                                ) : (
                                    <span></span>
                                )}
                                {data.has_kitchen ? (
                                    <span>, Phòng bếp</span>
                                ) : (
                                    <span></span>
                                )}
                                {data.has_airConditioner ? (
                                    <span>, Điều hòa</span>
                                ) : (
                                    <span></span>
                                )}
                                {data.has_waterHeater ? (
                                    <span>, Nóng lạnh</span>
                                ) : (
                                    <span></span>
                                )}
                            </p>
                        </div>
                        <div className='form-row'>
                            <i className='fas fa-credit-card'></i>
                            <p>
                                Dịch vụ: Giá điện: {data.electricity}
                                VNĐ/KWH, Giá nước: {data.water}VNĐ/M3
                            </p>
                        </div>
                    </div>
                    <div className='col-sm-6'>
                        <h2>Thông tin chủ trọ</h2>
                        <br />
                        <div className='form-row'>
                            <i className='fas fa-user-tag'></i>
                            <p>Họ Tên: {data.owner_name}</p>
                        </div>
                        <div className='form-row'>
                            <i className='fas fa-phone'></i>
                            <p>Số điện thoại: {data.owner_phone}</p>
                        </div>
                    </div>
                </div>
                <br />
                <Review />
                <br />
                <div>{showAllReviews()}</div>
                <br />
                <Report />
            </div>
            <br />
            {/* <Footer /> */}
        </div>
    );
};

export default PostDetailRenter;
