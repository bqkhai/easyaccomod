import React, { useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import dotenv from 'dotenv';
dotenv.config();

const Review = () => {
    const [data, setData] = useState({
        star: 0,
        review: '',
        star_checked: [false, false, false, false, false],
    });

    const handleChangeStar = (e) => {
        setData((data) => {
            const star = parseInt(e.target.value);
            const star_checked = data.star_checked.map((elem, index) => {
                if (index <= star - 1) {
                    elem = true;
                } else {
                    elem = false;
                }
                return elem;
            });
            return {
                star: star,
                star_checked: star_checked,
            };
        });
    };

    const handleChange = (e) => {
        setData({
            ...data,
            review: e.target.value,
        });
    };

    const { id } = useParams();

    const onSubmit = (e) => {
        e.preventDefault();

        const reviewData = {
            ...data,
        };

        delete data.star_checked;

        axios
            .post(
                `${process.env.REACT_APP_HOST_URL}/user/renter/review/${id}`,
                reviewData,
                {
                    withCredentials: true,
                }
            )
            .then((res) => {
                if (res.status === 201) {
                    alert('Hãy chờ đợi admin phê duyệt!');
                    window.location.reload();
                }
            })
            .catch((err) => console.log(err));
    };

    return (
        <div>
            <form method='POST' onSubmit={onSubmit}>
                <div className='form-group rating'>
                    <span>
                        <input
                            type='checkbox'
                            className='star'
                            name='star1'
                            id='star1'
                            value='1'
                            checked={data.star_checked[0]}
                            onChange={handleChangeStar}
                        ></input>
                    </span>
                    <span>
                        <input
                            type='checkbox'
                            className='star'
                            name='star2'
                            id='star2'
                            value='2'
                            checked={data.star_checked[1]}
                            onChange={handleChangeStar}
                        ></input>
                    </span>
                    <span>
                        <input
                            type='checkbox'
                            className='star'
                            name='star3'
                            id='star3'
                            value='3'
                            checked={data.star_checked[2]}
                            onChange={handleChangeStar}
                        ></input>
                    </span>
                    <span>
                        <input
                            type='checkbox'
                            className='star'
                            name='star4'
                            id='star4'
                            value='4'
                            checked={data.star_checked[3]}
                            onChange={handleChangeStar}
                        ></input>
                    </span>
                    <span>
                        <input
                            type='checkbox'
                            className='star'
                            name='star5'
                            id='star5'
                            value='5'
                            checked={data.star_checked[4]}
                            onChange={handleChangeStar}
                        ></input>
                    </span>
                </div>
                <div className='form-row'>
                    <div className='form-group col-sm-10'>
                        <input
                            className='form-control mr-auto'
                            id='review'
                            type='text'
                            value={data.review}
                            name='review'
                            onChange={handleChange}
                            placeholder='Suy nghĩ của bạn'
                        />
                    </div>
                    <button className='btn btn-info'>Bình luận</button>
                </div>
            </form>
        </div>
    );
};

export default Review;
