import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import dotenv from 'dotenv';
dotenv.config();

const Posts = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/renter/posts`, {
                withCredentials: true,
            })
            .then((res) => {
                console.log(res.data);
                setData(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const postsList = () => {
        return data.map((post, index) => {
            return (
                <div className='container'>
                    <div className='row mt-3 post'>
                        <div className='col-sm-4 img-demo'>
                            <Link to={`/user/renter/posts/${post._id}`}>
                                <img
                                    className='img-demo'
                                    src={post.image[0]}
                                    width='100%'
                                    alt='preview'
                                ></img>
                            </Link>
                        </div>
                        <div className='col-sm-5 info'>
                            <Link to={`/user/renter/posts/${post._id}`}>
                                <h3>{post.title}</h3>
                            </Link>
                            <p>
                                Địa chỉ: {post.street}, {post.ward},{' '}
                                {post.district}, {post.city}
                            </p>
                            {post.rented_status ? (
                                <p className='text-danger'>Đã có người thuê</p>
                            ) : (
                                <p className='text-success'>
                                    Chưa có người thuê
                                </p>
                            )}
                            <div className='form-row'>
                                <i className='fas fa-eye'></i>
                                <p>Bài đăng nhận {post.views} lượt xem</p>
                            </div>
                            <div className='form-row'>
                                <i className='fas fa-heart'></i>
                                <p>{post.likes} Lượt thích</p>
                            </div>
                        </div>
                        <div className='col-sm-3 rate'>
                            <p>
                                <span className='rented-rate'>
                                    {post.room_price} vnđ
                                </span>
                                /tháng
                            </p>
                            <Link to={`/user/renter/posts/${post._id}`}>
                                <button className='btn btn-info'>
                                    Xem phòng
                                </button>
                            </Link>
                        </div>
                    </div>
                    <br />
                    <br />
                </div>
            );
        });
    };
    return (
        <div>
            {/* <RenterNav /> */}
            <div>{postsList()}</div>
        </div>
    );
};

export default Posts;
