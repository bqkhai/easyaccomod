import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Footer from '../../components/footer/footer.component';

import dotenv from 'dotenv';
dotenv.config();

const Posts = () => {
    const [data, setData] = useState({
        city: '',
        district: '',
        ward: '',
        room_type: '',
        has_bathroom: false,
        has_kitchen: false,
        has_airConditioner: false,
        has_waterHeater: false,
        room_price: '',
        room_area: '',
        posts: [],
    });

    useEffect(() => {
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/renter/posts`, {
                withCredentials: true,
            })
            .then((res) => {
                console.log(res.data);
                setData({ posts: res.data });
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const handleChange = (e) => {
        const value =
            e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        setData({
            ...data,
            [e.target.name]: value,
        });
    };

    const onSubmit = (e) => {
        e.preventDefault();
        let params = {};
        for (const st in data) {
            const key = st;
            const value = data[st];
            if (value !== '' && value !== false) {
                params[key] = value;
            }
        }
        delete params.posts;
        console.log(params);
        const config = {
            params: {
                ...params,
            },
            withCredentials: true,
        };
        axios
            .get(`${process.env.REACT_APP_HOST_URL}/user/renter/search`, config)
            .then((res) => {
                setData({
                    posts: res.data,
                });
            })
            .catch((err) => console.log(err));
    };

    const postsList = () => {
        return data.posts.map((post, index) => {
            return (
                <div className='container'>
                    <div className='row mt-3 post'>
                        <div className='col-sm-4 img-demo'>
                            <Link to={`/user/renter/posts/${post._id}`}>
                                <img
                                    className='img-demo'
                                    src={post.image[0]}
                                    width='100%'
                                    alt='preview'
                                ></img>
                            </Link>
                        </div>
                        <div className='col-sm-5 xs-5 info'>
                            <Link to={`/user/renter/posts/${post._id}`}>
                                <h3>{post.title}</h3>
                            </Link>
                            <p>
                                Địa chỉ: {post.street}, {post.ward},{' '}
                                {post.district}, {post.city}
                            </p>
                            {post.rented_status ? (
                                <p className='text-danger'>Đã có người thuê</p>
                            ) : (
                                <p className='text-success'>
                                    Chưa có người thuê
                                </p>
                            )}
                            <div className='form-row'>
                                <i className='fas fa-eye'></i>
                                <p>Bài đăng nhận {post.views} lượt xem</p>
                            </div>
                            <div className='form-row'>
                                <i className='fas fa-heart'></i>
                                <p>{post.likes} Lượt thích</p>
                            </div>
                        </div>
                        <div className='col-sm-3 xs-3 rate'>
                            <p>
                                <span className='rented-rate'>
                                    {post.room_price} vnđ
                                </span>
                                /tháng
                            </p>
                            <Link to={`/user/renter/posts/${post._id}`}>
                                <button className='btn btn-info'>
                                    Xem phòng
                                </button>
                            </Link>
                        </div>
                    </div>
                    <br />
                    <br />
                </div>
            );
        });
    };
    return (
        <div className='content'>
            <br />
            <form method='GET' onSubmit={onSubmit} className='search-bar'>
                <div className='container'>
                    <div className='form-row'>
                        <div className='form-group col-sm-2'>
                            <input
                                className='form-control col-sm-11'
                                id='city'
                                type='text'
                                name='city'
                                value={data.city}
                                onChange={handleChange}
                                placeholder='Thành phố'
                            />
                        </div>
                        <div className='form-group col-sm-2'>
                            <input
                                className='form-control col-sm-11'
                                id='district'
                                type='text'
                                name='district'
                                value={data.district}
                                onChange={handleChange}
                                placeholder='Quận'
                            />
                        </div>
                        <div className='form-group col-sm-2'>
                            <input
                                className='form-control col-sm-11'
                                id='ward'
                                type='text'
                                name='ward'
                                value={data.ward}
                                onChange={handleChange}
                                placeholder='Phường'
                            />
                        </div>
                        <div className='form-group col-sm-2'>
                            <select
                                className='form-control col-sm-11'
                                id='room_type'
                                type='text'
                                name='room_type'
                                value={data.room_type}
                                onChange={handleChange}
                            >
                                <option>Nhà nguyên căn</option>
                                <option>Chung cư mini</option>
                                <option>Chung cư</option>
                                <option>Phòng trọ</option>
                                <option>Nhà trọ giá rẻ</option>
                            </select>
                        </div>
                        <div className='form-group col-sm-2'>
                            <input
                                className='form-control col-sm-11'
                                id='room_price'
                                type='number'
                                name='room_price'
                                value={data.room_price}
                                onChange={handleChange}
                                placeholder='Giá thuê'
                            />
                        </div>
                    </div>
                    <div className='form-row'>
                        <div className='form-group col-sm-2'>
                            <input
                                className='form-control col-sm-11'
                                id='room_area'
                                type='number'
                                name='room_area'
                                value={data.room_area}
                                onChange={handleChange}
                                placeholder='Diện tích'
                            />
                        </div>
                        <div className='col-sm-2'>
                            <div className='form-group col-sm-11 facilities'>
                                <label htmlFor='bathroom'>Phòng tắm</label>
                                <input
                                    className='pro-checkbox'
                                    id='has_bathroom'
                                    type='checkbox'
                                    name='has_bathroom'
                                    value={data.has_bathroom}
                                    onChange={handleChange}
                                />
                            </div>
                        </div>
                        <div className='col-sm-2'>
                            <div className='form-group col-sm-11 facilities'>
                                <label htmlFor='has_kitchen'>Bếp</label>
                                <input
                                    className='pro-checkbox'
                                    id='has_kitchen'
                                    type='checkbox'
                                    name='has_kitchen'
                                    value={data.has_kitchen}
                                    onChange={handleChange}
                                />
                            </div>
                        </div>
                        <div className='col-sm-2'>
                            <div className='form-group col-sm-11 facilities'>
                                <label htmlFor='has_airConditioner'>
                                    Điều hòa
                                </label>
                                <input
                                    className='pro-checkbox'
                                    id='has_airConditioner'
                                    type='checkbox'
                                    name='has_airConditioner'
                                    value={data.has_airConditioner}
                                    onChange={handleChange}
                                />
                            </div>
                        </div>
                        <div className='col-sm-2'>
                            <div className='form-group col-sm-11 facilities'>
                                <label htmlFor='has_waterHeater'>
                                    Nóng lạnh
                                </label>
                                <input
                                    className='pro-checkbox'
                                    id='has_waterHeater'
                                    type='checkbox'
                                    name='has_waterHeater'
                                    value={data.has_waterHeater}
                                    onChange={handleChange}
                                />
                            </div>
                        </div>
                        <div className='col-sm-2 text-center'>
                            <button className='btn btn-info col-sm-10'>
                                Tìm
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <br />
            <div>{postsList()}</div>
            <br />
            <br />
            <br />
            <Footer />
        </div>
    );
};

export default Posts;
