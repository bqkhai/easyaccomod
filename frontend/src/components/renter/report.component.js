import React, { useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import dotenv from 'dotenv';
dotenv.config();

const Report = () => {
    const [report, setData] = useState('');

    const handleChange = (e) => {
        setData(e.target.value);
    };

    const { id } = useParams();

    const onSubmit = (e) => {
        e.preventDefault();

        const data = { report };

        axios
            .post(
                `${process.env.REACT_APP_HOST_URL}/user/renter/report/${id}`,
                data,
                {
                    withCredentials: true,
                }
            )
            .then((res) => {
                if (res.status === 201) {
                    alert('Bạn đã gửi báo cáo thành công cho Admin');
                }
                window.location.reload();
                console.log(res.data);
            })
            .catch((err) => console.log(err));
    };

    return (
        <div>
            <form method='POST' onSubmit={onSubmit}>
                <div className='form-row'>
                    <div className='form-group col-sm-10'>
                        <input
                            type='text'
                            className='form-control'
                            id='report'
                            name='report'
                            value={report}
                            onChange={handleChange}
                            placeholder='Báo cáo bài đăng'
                        ></input>
                    </div>
                    <button className='btn btn-danger'>Báo cáo</button>
                </div>
            </form>
        </div>
    );
};

export default Report;
