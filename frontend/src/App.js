import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Navigate,
    Redirect,
} from 'react-router-dom';
import './css/app.css';

import Home from './components/home/home.component';
import Login from './components/users/login.component';
import Signup from './components/users/signup.component';
import SignupOwner from './components/users/signup/owner.component';
import SignupRenter from './components/users/signup/renter.component';
import ChangePassword from './components/users/change_pass.component';

import AdminWS from './components/workspace/admin.component';
import OwnerWS from './components/workspace/owner.component';
import RenterWS from './components/workspace/renter.component';

/**
 * Admin
 */
import ManageAccounts from './components/admin/accounts.component';
import ManagePosts from './components/admin/posts.component';
import PostDetail from './components/admin/post_detail.component';
import NotificationAdmin from './components/admin/notification.component';
import ManageReports from './components/admin/reports.component';
import ManageReviews from './components/admin/reviews.component';
import ManagerEditAuth from './components/admin/edit_auth.component';

/**
 * Owner
 */
import AllPosts from './components/owner/all_posts.component';
import CreatePost from './components/owner/create_post.component';
import EditPost from './components/owner/edit_post.component';
import ExtendPost from './components/owner/extend_post.component';
import NotificationOwner from './components/owner/notification.component';
import ChangeInfo from './components/owner/change_info.component';

/**
 * Renter
 */
import FavouritePosts from './components/renter/favourite_posts.component';
import Posts from './components/renter/list_posts.component';
import PostDetailRenter from './components/renter/post_detail.component';

function App() {
    return (
        <Router>
            {document.cookie === '' ? (
                <Routes>
                    <Route path='/' element={<Home />}></Route>
                    <Route path='/login' element={<Login />}></Route>
                    <Route path='/signup' element={<Signup />}></Route>
                    <Route
                        path='/signup/owner'
                        element={<SignupOwner />}
                    ></Route>
                    <Route
                        path='/signup/renter'
                        element={<SignupRenter />}
                    ></Route>
                    {/* <Navigate to='/login'></Navigate> */}
                </Routes>
            ) : null}
            {localStorage.getItem('user_role') === 'renter' &&
            document.cookie !== '' ? (
                <Routes>
                    <Route path='user/renter' element={<RenterWS />}></Route>
                    <Route path='user/posts' element={<Posts />}></Route>
                    <Route
                        path='user/renter/favourite'
                        element={<FavouritePosts />}
                    ></Route>
                    <Route
                        path='user/renter/posts/:id'
                        element={<PostDetailRenter />}
                    ></Route>
                    <Route
                        path='user/change-password'
                        element={<ChangePassword />}
                    ></Route>
                </Routes>
            ) : localStorage.getItem('user_role') === 'owner' &&
              document.cookie !== '' ? (
                <Routes>
                    <Route path='user/owner' element={<OwnerWS />}></Route>
                    <Route
                        path='user/owner/posts'
                        element={<AllPosts />}
                    ></Route>
                    <Route
                        path='user/owner/post'
                        element={<CreatePost />}
                    ></Route>
                    <Route
                        path='user/owner/edit/:id'
                        element={<EditPost />}
                    ></Route>
                    <Route
                        path='user/owner/notification'
                        element={<NotificationOwner />}
                    ></Route>
                    <Route
                        path='user/owner/extend'
                        element={<ExtendPost />}
                    ></Route>
                    <Route
                        path='user/change-password'
                        element={<ChangePassword />}
                    ></Route>
                    <Route
                        path='user/change-info'
                        element={<ChangeInfo />}
                    ></Route>
                    {/* <Navigate to='/user/owner'></Navigate> */}
                </Routes>
            ) : localStorage.getItem('user_role') === 'admin' &&
              document.cookie !== '' ? (
                <Routes>
                    <Route path='user/admin' element={<AdminWS />}></Route>
                    <Route
                        path='user/admin/notification'
                        element={<NotificationAdmin />}
                    ></Route>
                    <Route
                        path='user/admin/accounts'
                        element={<ManageAccounts />}
                    ></Route>
                    <Route
                        path='user/admin/posts'
                        element={<ManagePosts />}
                    ></Route>
                    <Route
                        path='user/admin/posts/:id'
                        element={<PostDetail />}
                    ></Route>
                    <Route
                        path='user/admin/reports'
                        element={<ManageReports />}
                    ></Route>
                    <Route
                        path='user/admin/reviews'
                        element={<ManageReviews />}
                    ></Route>
                    <Route
                        path='user/admin/edit-auth'
                        element={<ManagerEditAuth />}
                    ></Route>
                    <Route
                        path='user/change-password'
                        element={<ChangePassword />}
                    ></Route>
                </Routes>
            ) : null}
        </Router>
    );
}

export default App;
