# EasyAccomod ~ House For Rent
```
cd existing_repo
git remote add origin https://gitlab.com/bqkhai/easyaccomod.git
git branch -M main
git push -uf origin main
```

# Description
+ Website tìm phòng trọ
+ Tech: 
    - NodeJS/ ExpressJS, MongoDB/ Mongoose, ReactJS <Hooks>, CSS, Bootstrap...
    - Authenciation cookies, cors
    - Database store on Mongo Atlas
    - Images store on Cloudinary
+ Admin (Quản lý)
    - Duyệt tài khoản
    - Duyệt bài đăng mới, bài chỉnh sửa, bài gia hạn
    - Xem chi tiết bài đăng của owner
    - Nhận thông báo thay đổi trạng thái thuê phòng từ owner
    - Thông báo duyệt bài cho owner
    - Duyệt các bình luận -> Hiển thị
    - Nhận báo cáo của renter về bài đăng
    - Thay đổi mật khẩu
    - Cấp quyền chỉnh sửa tài khoản cho owner
+ Owner (Chủ trọ)
    - Đăng ký tài khoản -> Chờ duyệt
    - Xin admin quyền chỉnh sửa -> Chỉnh sửa thông tin tài khoản -> Chờ duyệt
    - Thay đổi mật khẩu
    - Tạo bài đăng -> Chờ Admin duyệt -> Hiển thị cho Renter
    - Chỉnh sửa bài đăng, gia hạn bài hết thời gian đăng -> Chờ Admin duyệt
    - Thay đổi trạng thái phòng đã được thuê hay chưa được thuê
    - Thông báo cho Admin khi thay đổi trạng thái thuê phòng
    - Nhận thông báo Admin duyệt bài cho mình
+ Renter (Người tìm phòng trọ)
    - Đăng ký, đổi mật khẩu
    - Xem danh sách bài đăng 
    - Tìm kiếm
    - Xem chi tiết bài đăng
    - Thêm vào danh sách yêu thích

# Install
+ Clone project: git clone https://gitlab.com/bqkhai/easyaccomod.git
+ Database:
    - Create MongoDB Atlas account or install mongoDB in local
    - Create database
    - Create .env file
    - Change enviroments in .env file same as .env.example file
+ Create Cloudinary account
    - Change enviroments in .env file same as .env.example file
+ cd backend -> npm install -> npm start || npm run server -> http://localhost:5000
+ cd frontend -> npm install -> npm start -> http://localhost:3000

# Folder structure
```
+---backend
|   |   .dockerignore
|   |   .env
|   |   .env.example
|   |   .gitignore
|   |   Dockerfile
|   |   index.js
|   |   package-lock.json
|   |   package.json
|   |   request.http
|   |   
|   +---config
|   |       cloudinary.js
|   |       config.js
|   |       multer.js
|   |       
|   +---controllers
|   |       admin.controller.js
|   |       login.controller.js
|   |       logout.controller.js
|   |       owner.controller.js
|   |       renter.controller.js
|   |       signup.controller.js
|   |       user.controller.js
|   |       
|   +---middlewares
|   |       account.middleware.js
|   |       auth.middleware.js
|   |       post.middleware.js
|   |       role.middleware.js
|   |       
|   +---models
|   |       favouritePost.model.js
|   |       notif.model.js
|   |       post.model.js
|   |       report.model.js
|   |       review.model.js
|   |       user.model.js
|   |       
|   +---routes
|   |       admin.route.js
|   |       login.route.js
|   |       logout.route.js
|   |       owner.route.js
|   |       renter.route.js
|   |       signup.route.js
|   |       user.route.js
|   |       
|   +---static
|   |           
|   +---uploads
|   |
+---frontend
    |   .dockerignore
    |   .env
    |   .env.example
    |   .gitignore
    |   Dockerfile
    |   package-lock.json
    |   package.json
    |   README.md
    |   
    +---public
    +---src
        |   App.css
        |   App.js
        |   App.test.js
        |   index.css
        |   index.js
        |   logo.svg
        |   reportWebVitals.js
        |   setupTests.js
        |   
        +---components
        |   +---admin
        |   |       accounts.component.js
        |   |       edit_auth.component.js
        |   |       notification.component.js
        |   |       posts.component.js
        |   |       post_detail.component.js
        |   |       reports.component.js
        |   |       reviews.component.js
        |   |       
        |   +---common
        |   |       admin_nav.component.js
        |   |       owner_nav.component.js
        |   |       renter_nav.component.js
        |   |       
        |   +---footer
        |   |       footer.component.js
        |   |       footer.css
        |   |       
        |   +---home
        |   |       home.component.js
        |   |       home.css
        |   |       
        |   +---navbar
        |   |       navbar.component.js
        |   |       navbar.css
        |   |       
        |   +---owner
        |   |       all_posts.component.js
        |   |       change_info.component.js
        |   |       create_post.component.js
        |   |       edit_post.component.js
        |   |       extend_post.component.js
        |   |       notification.component.js
        |   |       
        |   +---renter
        |   |       favourite_posts.component.js
        |   |       list_posts.component.js
        |   |       post_detail.component.js
        |   |       report.component.js
        |   |       review.component.js
        |   |       search.component.js
        |   |       
        |   +---users
        |   |   |   change_pass.component.js
        |   |   |   login.component.js
        |   |   |   logout.component.js
        |   |   |   signup.component.js
        |   |   |   
        |   |   +---signup
        |   |           owner.component.js
        |   |           renter.component.js
        |   |           
        |   +---workspace
        |           admin.component.js
        |           owner.component.js
        |           renter.component.js
        |           
        +---config
        +---css     
        +---images 
        +---utils

```
